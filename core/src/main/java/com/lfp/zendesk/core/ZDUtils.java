package com.lfp.zendesk.core;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.asynchttpclient.Request;
import org.asynchttpclient.Response;

import com.github.throwable.beanref.BeanPath;
import com.github.throwable.beanref.BeanRef;
import com.google.re2j.Pattern;
import com.lfp.joe.core.classpath.CoreReflections;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.net.email.Emails;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import at.favre.lib.bytes.Bytes;
import io.netty.handler.codec.http.HttpHeaders;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

@SuppressWarnings("unchecked")
public class ZDUtils {

	private static final String DEFAULT_USER_NAME = "Support User";

	public static String generateUserName(String emailAddress, String name) {
		{// remove any emails in name
			for (var addr : Emails.parseAddresses(name).toSet())
				name = Utils.Strings.replaceIgnoreCase(name, addr, "");
		}
		{// remove empty brackets in the name
			name = Utils.Strings.replaceIgnoreCase(name, "{}", "");
			name = Utils.Strings.replaceIgnoreCase(name, "[]", "");
			name = Utils.Strings.replaceIgnoreCase(name, "()", "");
			name = Utils.Strings.replaceIgnoreCase(name, "<>", "");
			name = Utils.Strings.replaceIgnoreCase(name, "''", "");
			name = Utils.Strings.replaceIgnoreCase(name, "\"\"", "");
		}
		var parsed = Emails.parse(String.format("%s <%s>", name, emailAddress)).orElse(null);
		name = parsed == null ? null : parsed.getName();
		if (Utils.Strings.isBlank(name)) {
			name = DEFAULT_USER_NAME;
			if (parsed != null)
				name += String.format(" [%s]", parsed.getAddressDomain());
		}
		return name;
	}

	public static <T> String getSummary(Request request, Response response) {
		return getSummary(request, response.getStatusCode(), response.getHeaders());
	}

	public static <T> String getSummary(Request request, Integer responseStatusCode, HttpHeaders responseHeaders) {
		Map<String, Object> data = new LinkedHashMap<>();
		data.put("responseStatusCode", responseStatusCode);
		data.put("requestURL", request.getUri());
		data.put("requestMethod", request.getMethod());
		data.put("requestBody", getRequestBody(request));
		data.put("responseHeaders", HeaderMap.of(responseHeaders).map());
		data = Utils.Lots.stream(data).nonNullValues().toCustomMap(LinkedHashMap::new);
		return Serials.Gsons.get().toJson(data);
	}

	public static Bytes getRequestBody(Request request) {
		if (request == null)
			return Utils.Bits.empty();
		List<ThrowingSupplier<Bytes, IOException>> bodySuppliers = new ArrayList<>();
		bodySuppliers.add(() -> {
			var data = request.getStreamData();
			if (data == null)
				return null;
			try (data) {
				return Utils.Bits.from(data);
			}
		});
		bodySuppliers.add(() -> {
			var data = request.getByteData();
			if (data == null)
				return null;
			return Utils.Bits.from(data);
		});
		bodySuppliers.add(() -> {
			var data = request.getByteBufferData();
			if (data == null)
				return null;
			return Utils.Bits.from(data);
		});
		bodySuppliers.add(() -> {
			var data = request.getStringData();
			return Utils.Strings.trimToNullOptional(data).map(Utils.Bits::from).orElse(null);
		});
		for (var bodySupplier : bodySuppliers) {
			try {
				var bytes = bodySupplier.get();
				if (bytes != null && bytes.length() > 0)
					return bytes;
			} catch (IOException e) {
				// suppress
			}
		}
		return Utils.Bits.empty();
	}

	public static <X> EntryStream<BeanPath<X, Date>, Date> streamDateValues(X bean) {
		StreamEx<BeanPath<X, Date>> stream = streamDatePaths(
				bean == null ? null : (Class<? extends X>) bean.getClass());
		return stream.mapToEntry(v -> v.get(bean)).nonNullValues();
	}

	public static <X> StreamEx<BeanPath<X, Date>> streamDatePaths(Class<? extends X> classType) {
		if (classType == null)
			return StreamEx.empty();
		classType = CoreReflections.getNamedClassType(classType);
		var ctStream = JavaCode.Reflections.streamSuperTypes(classType, true);
		ctStream = ctStream.filter(v -> {
			var packageName = v.getPackageName();
			if (Utils.Strings.isBlank(packageName))
				return false;
			return Pattern.compile("\\bzendesk\\b").matcher(packageName).find();
		});
		if (!ctStream.findFirst().isPresent())
			return StreamEx.empty();
		var bps = BeanRef.$(classType).all();
		var stream = Utils.Lots.stream(bps).filter(v -> Date.class.isAssignableFrom(v.getType()));
		stream = stream.distinct();
		return stream.map(v -> (BeanPath<X, Date>) v);
	}

	public static void main(String[] args) {
		var found = Pattern.compile("\\bzendesk\\b").matcher("zzendesk.cool").find();
		System.out.println(found);

		System.out.println(generateUserName("reggie.pierce@gmail.com", ""));
	}

}
