
package com.lfp.zendesk.core.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class MetricSet {

	@SerializedName("url")
	@Expose
	private String url;
	@SerializedName("id")
	@Expose
	private Long id;
	@SerializedName("ticket_id")
	@Expose
	private Integer ticketId;
	@SerializedName("created_at")
	@Expose
	private Date createdAt;
	@SerializedName("updated_at")
	@Expose
	private Date updatedAt;
	@SerializedName("group_stations")
	@Expose
	private Integer groupStations;
	@SerializedName("assignee_stations")
	@Expose
	private Integer assigneeStations;
	@SerializedName("reopens")
	@Expose
	private Integer reopens;
	@SerializedName("replies")
	@Expose
	private Integer replies;
	@SerializedName("assignee_updated_at")
	@Expose
	private Date assigneeUpdatedAt;
	@SerializedName("requester_updated_at")
	@Expose
	private Date requesterUpdatedAt;
	@SerializedName("status_updated_at")
	@Expose
	private Date statusUpdatedAt;
	@SerializedName("initially_assigned_at")
	@Expose
	private Date initiallyAssignedAt;
	@SerializedName("assigned_at")
	@Expose
	private Date assignedAt;
	@SerializedName("solved_at")
	@Expose
	private Date solvedAt;
	@SerializedName("latest_comment_added_at")
	@Expose
	private Date latestCommentAddedAt;
	@SerializedName("reply_time_in_minutes")
	@Expose
	private Minutes replyTimeInMinutes;
	@SerializedName("first_resolution_time_in_minutes")
	@Expose
	private Minutes firstResolutionTimeInMinutes;
	@SerializedName("full_resolution_time_in_minutes")
	@Expose
	private Minutes fullResolutionTimeInMinutes;
	@SerializedName("agent_wait_time_in_minutes")
	@Expose
	private Minutes agentWaitTimeInMinutes;
	@SerializedName("requester_wait_time_in_minutes")
	@Expose
	private Minutes requesterWaitTimeInMinutes;
	@SerializedName("on_hold_time_in_minutes")
	@Expose
	private Minutes onHoldTimeInMinutes;

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public MetricSet withUrl(String url) {
		this.url = url;
		return this;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public MetricSet withId(Long id) {
		this.id = id;
		return this;
	}

	public Integer getTicketId() {
		return ticketId;
	}

	public void setTicketId(Integer ticketId) {
		this.ticketId = ticketId;
	}

	public MetricSet withTicketId(Integer ticketId) {
		this.ticketId = ticketId;
		return this;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public MetricSet withCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
		return this;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public MetricSet withUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
		return this;
	}

	public Integer getGroupStations() {
		return groupStations;
	}

	public void setGroupStations(Integer groupStations) {
		this.groupStations = groupStations;
	}

	public MetricSet withGroupStations(Integer groupStations) {
		this.groupStations = groupStations;
		return this;
	}

	public Integer getAssigneeStations() {
		return assigneeStations;
	}

	public void setAssigneeStations(Integer assigneeStations) {
		this.assigneeStations = assigneeStations;
	}

	public MetricSet withAssigneeStations(Integer assigneeStations) {
		this.assigneeStations = assigneeStations;
		return this;
	}

	public Integer getReopens() {
		return reopens;
	}

	public void setReopens(Integer reopens) {
		this.reopens = reopens;
	}

	public MetricSet withReopens(Integer reopens) {
		this.reopens = reopens;
		return this;
	}

	public Integer getReplies() {
		return replies;
	}

	public void setReplies(Integer replies) {
		this.replies = replies;
	}

	public MetricSet withReplies(Integer replies) {
		this.replies = replies;
		return this;
	}

	public Date getAssigneeUpdatedAt() {
		return assigneeUpdatedAt;
	}

	public void setAssigneeUpdatedAt(Date assigneeUpdatedAt) {
		this.assigneeUpdatedAt = assigneeUpdatedAt;
	}

	public MetricSet withAssigneeUpdatedAt(Date assigneeUpdatedAt) {
		this.assigneeUpdatedAt = assigneeUpdatedAt;
		return this;
	}

	public Date getRequesterUpdatedAt() {
		return requesterUpdatedAt;
	}

	public void setRequesterUpdatedAt(Date requesterUpdatedAt) {
		this.requesterUpdatedAt = requesterUpdatedAt;
	}

	public MetricSet withRequesterUpdatedAt(Date requesterUpdatedAt) {
		this.requesterUpdatedAt = requesterUpdatedAt;
		return this;
	}

	public Date getStatusUpdatedAt() {
		return statusUpdatedAt;
	}

	public void setStatusUpdatedAt(Date statusUpdatedAt) {
		this.statusUpdatedAt = statusUpdatedAt;
	}

	public MetricSet withStatusUpdatedAt(Date statusUpdatedAt) {
		this.statusUpdatedAt = statusUpdatedAt;
		return this;
	}

	public Date getInitiallyAssignedAt() {
		return initiallyAssignedAt;
	}

	public void setInitiallyAssignedAt(Date initiallyAssignedAt) {
		this.initiallyAssignedAt = initiallyAssignedAt;
	}

	public MetricSet withInitiallyAssignedAt(Date initiallyAssignedAt) {
		this.initiallyAssignedAt = initiallyAssignedAt;
		return this;
	}

	public Date getAssignedAt() {
		return assignedAt;
	}

	public void setAssignedAt(Date assignedAt) {
		this.assignedAt = assignedAt;
	}

	public MetricSet withAssignedAt(Date assignedAt) {
		this.assignedAt = assignedAt;
		return this;
	}

	public Date getSolvedAt() {
		return solvedAt;
	}

	public void setSolvedAt(Date solvedAt) {
		this.solvedAt = solvedAt;
	}

	public MetricSet withSolvedAt(Date solvedAt) {
		this.solvedAt = solvedAt;
		return this;
	}

	public Date getLatestCommentAddedAt() {
		return latestCommentAddedAt;
	}

	public void setLatestCommentAddedAt(Date latestCommentAddedAt) {
		this.latestCommentAddedAt = latestCommentAddedAt;
	}

	public MetricSet withLatestCommentAddedAt(Date latestCommentAddedAt) {
		this.latestCommentAddedAt = latestCommentAddedAt;
		return this;
	}

	public Minutes getReplyTimeInMinutes() {
		return replyTimeInMinutes;
	}

	public void setReplyTimeInMinutes(Minutes replyTimeInMinutes) {
		this.replyTimeInMinutes = replyTimeInMinutes;
	}

	public MetricSet withReplyTimeInMinutes(Minutes replyTimeInMinutes) {
		this.replyTimeInMinutes = replyTimeInMinutes;
		return this;
	}

	public Minutes getFirstResolutionTimeInMinutes() {
		return firstResolutionTimeInMinutes;
	}

	public void setFirstResolutionTimeInMinutes(Minutes firstResolutionTimeInMinutes) {
		this.firstResolutionTimeInMinutes = firstResolutionTimeInMinutes;
	}

	public MetricSet withFirstResolutionTimeInMinutes(Minutes firstResolutionTimeInMinutes) {
		this.firstResolutionTimeInMinutes = firstResolutionTimeInMinutes;
		return this;
	}

	public Minutes getFullResolutionTimeInMinutes() {
		return fullResolutionTimeInMinutes;
	}

	public void setFullResolutionTimeInMinutes(Minutes fullResolutionTimeInMinutes) {
		this.fullResolutionTimeInMinutes = fullResolutionTimeInMinutes;
	}

	public MetricSet withFullResolutionTimeInMinutes(Minutes fullResolutionTimeInMinutes) {
		this.fullResolutionTimeInMinutes = fullResolutionTimeInMinutes;
		return this;
	}

	public Minutes getAgentWaitTimeInMinutes() {
		return agentWaitTimeInMinutes;
	}

	public void setAgentWaitTimeInMinutes(Minutes agentWaitTimeInMinutes) {
		this.agentWaitTimeInMinutes = agentWaitTimeInMinutes;
	}

	public MetricSet withAgentWaitTimeInMinutes(Minutes agentWaitTimeInMinutes) {
		this.agentWaitTimeInMinutes = agentWaitTimeInMinutes;
		return this;
	}

	public Minutes getRequesterWaitTimeInMinutes() {
		return requesterWaitTimeInMinutes;
	}

	public void setRequesterWaitTimeInMinutes(Minutes requesterWaitTimeInMinutes) {
		this.requesterWaitTimeInMinutes = requesterWaitTimeInMinutes;
	}

	public MetricSet withRequesterWaitTimeInMinutes(Minutes requesterWaitTimeInMinutes) {
		this.requesterWaitTimeInMinutes = requesterWaitTimeInMinutes;
		return this;
	}

	public Minutes getOnHoldTimeInMinutes() {
		return onHoldTimeInMinutes;
	}

	public void setOnHoldTimeInMinutes(Minutes onHoldTimeInMinutes) {
		this.onHoldTimeInMinutes = onHoldTimeInMinutes;
	}

	public MetricSet withOnHoldTimeInMinutes(Minutes onHoldTimeInMinutes) {
		this.onHoldTimeInMinutes = onHoldTimeInMinutes;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("url", url).append("id", id).append("ticketId", ticketId)
				.append("createdAt", createdAt).append("updatedAt", updatedAt).append("groupStations", groupStations)
				.append("assigneeStations", assigneeStations).append("reopens", reopens).append("replies", replies)
				.append("assigneeUpdatedAt", assigneeUpdatedAt).append("requesterUpdatedAt", requesterUpdatedAt)
				.append("statusUpdatedAt", statusUpdatedAt).append("initiallyAssignedAt", initiallyAssignedAt)
				.append("assignedAt", assignedAt).append("solvedAt", solvedAt)
				.append("latestCommentAddedAt", latestCommentAddedAt).append("replyTimeInMinutes", replyTimeInMinutes)
				.append("firstResolutionTimeInMinutes", firstResolutionTimeInMinutes)
				.append("fullResolutionTimeInMinutes", fullResolutionTimeInMinutes)
				.append("agentWaitTimeInMinutes", agentWaitTimeInMinutes)
				.append("requesterWaitTimeInMinutes", requesterWaitTimeInMinutes)
				.append("onHoldTimeInMinutes", onHoldTimeInMinutes).toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(requesterUpdatedAt).append(latestCommentAddedAt)
				.append(firstResolutionTimeInMinutes).append(reopens).append(assignedAt).append(statusUpdatedAt)
				.append(requesterWaitTimeInMinutes).append(solvedAt).append(url).append(assigneeUpdatedAt)
				.append(createdAt).append(groupStations).append(replies).append(assigneeStations)
				.append(fullResolutionTimeInMinutes).append(initiallyAssignedAt).append(agentWaitTimeInMinutes)
				.append(id).append(onHoldTimeInMinutes).append(ticketId).append(updatedAt).append(replyTimeInMinutes)
				.toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof MetricSet) == false) {
			return false;
		}
		MetricSet rhs = ((MetricSet) other);
		return new EqualsBuilder().append(requesterUpdatedAt, rhs.requesterUpdatedAt)
				.append(latestCommentAddedAt, rhs.latestCommentAddedAt)
				.append(firstResolutionTimeInMinutes, rhs.firstResolutionTimeInMinutes).append(reopens, rhs.reopens)
				.append(assignedAt, rhs.assignedAt).append(statusUpdatedAt, rhs.statusUpdatedAt)
				.append(requesterWaitTimeInMinutes, rhs.requesterWaitTimeInMinutes).append(solvedAt, rhs.solvedAt)
				.append(url, rhs.url).append(assigneeUpdatedAt, rhs.assigneeUpdatedAt).append(createdAt, rhs.createdAt)
				.append(groupStations, rhs.groupStations).append(replies, rhs.replies)
				.append(assigneeStations, rhs.assigneeStations)
				.append(fullResolutionTimeInMinutes, rhs.fullResolutionTimeInMinutes)
				.append(initiallyAssignedAt, rhs.initiallyAssignedAt)
				.append(agentWaitTimeInMinutes, rhs.agentWaitTimeInMinutes).append(id, rhs.id)
				.append(onHoldTimeInMinutes, rhs.onHoldTimeInMinutes).append(ticketId, rhs.ticketId)
				.append(updatedAt, rhs.updatedAt).append(replyTimeInMinutes, rhs.replyTimeInMinutes).isEquals();
	}

}
