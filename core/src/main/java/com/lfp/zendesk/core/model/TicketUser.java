package com.lfp.zendesk.core.model;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TicketUser {

	public static TicketUser create(String emailAddress) {
		return create(emailAddress, null);
	}

	public static TicketUser create(String emailAddress, String name) {
		TicketUser tu = new TicketUser();
		tu.setUserEmail(emailAddress);
		tu.setUserName(StringUtils.trimToNull(name));
		return tu;
	}

	public static TicketUser create(Long userId) {
		TicketUser tu = new TicketUser();
		tu.setUserId(userId);
		return tu;
	}

	public static enum Action {
		put, delete;
	}

	private Long userId;

	private String userEmail;

	private String userName;

	private Action action;

	@JsonProperty("user_id")
	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@JsonProperty("user_email")
	public String getUserEmail() {
		return userEmail;
	}

	public void setUserEmail(String userEmail) {
		this.userEmail = userEmail;
	}

	@JsonProperty("user_name")
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	@JsonProperty("action")
	public Action getAction() {
		return action;
	}

	public void setAction(Action action) {
		this.action = action;
	}

}
