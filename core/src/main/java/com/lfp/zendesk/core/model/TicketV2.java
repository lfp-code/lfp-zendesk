package com.lfp.zendesk.core.model;


import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import com.lfp.joe.net.email.Emails;
import com.lfp.joe.utils.Utils;
import com.lfp.zendesk.core.ZDUtils;

import org.zendesk.client.v2.model.Ticket;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@SuppressWarnings("serial")
public class TicketV2 extends Ticket {
	
	public static final Comparator<Ticket> UPDATED_AT_COMPARATOR = Comparator.comparing(ticket -> {
		if (ticket == null)
			return Long.MAX_VALUE;
		Date updatedAt;
		if (ticket instanceof TicketV2)
			updatedAt = ((TicketV2) ticket).getUpdatedAt(true);
		else
			updatedAt = ticket.getUpdatedAt();
		updatedAt = Optional.ofNullable(updatedAt).orElse(new Date(0));
		return updatedAt.getTime() * -1;
	});

	private List<TicketUser> emailCcs;

	private List<TicketUser> followers;

	private List<Long> followerIds;

	private List<Long> emailCcIds;

	private List<? extends Object> additionalCollaborators;

	private MetricSet metricSet;

	private Long generatedTimestamp;

	@JsonIgnore
	public Date getUpdatedAt(boolean includeGeneratedTimestamp) {
		if (!includeGeneratedTimestamp)
			return super.getUpdatedAt();
		var generatedTimestampDate = Optional.ofNullable(this.generatedTimestamp).map(v -> v * 1000)
				.map(v -> new Date(v)).orElse(null);
		return Utils.Lots.stream(super.getUpdatedAt(), generatedTimestampDate).nonNull().sortedBy(v -> -1 * v.getTime())
				.findFirst().orElse(null);
	}

	@JsonProperty("email_ccs")
	public List<TicketUser> getEmailCcs() {
		return emailCcs;
	}

	public void setEmailCcs(List<TicketUser> emailCcs) {
		this.emailCcs = emailCcs;
	}

	@JsonProperty("followers")
	public List<TicketUser> getFollowers() {
		return followers;
	}

	public void setFollowers(List<TicketUser> followers) {
		this.followers = followers;
	}

	@JsonProperty("email_cc_ids")
	public List<Long> getEmailCcIds() {
		return emailCcIds;
	}

	public void setEmailCcIds(List<Long> emailCcIds) {
		this.emailCcIds = emailCcIds;
	}

	@JsonProperty("follower_ids")
	public List<Long> getFollowerIds() {
		return followerIds;
	}

	public void setFollowerIds(List<Long> followerIds) {
		this.followerIds = followerIds;
	}

	@JsonProperty("additional_collaborators")
	public List<? extends Object> getAdditionalCollaborators() {
		return additionalCollaborators;
	}

	public void setAdditionalCollaborators(Iterable<? extends Object> additionalCollaborators) {
		this.additionalCollaborators = Utils.Lots.stream(additionalCollaborators).nonNull().toList();
	}

	@JsonIgnore
	public void setAdditionalCollaborators(Map<String, String> emailAddressToName) {
		if (emailAddressToName == null || emailAddressToName.isEmpty()) {
			this.additionalCollaborators = List.of();
			return;
		}
		var stream = Utils.Lots.stream(emailAddressToName).map(ent -> {
			var toParse = String.format("%s <%s>", Optional.ofNullable(ent.getValue()).orElse(""),
					Optional.ofNullable(ent.getKey()).orElse(""));
			return Emails.parse(toParse).orElse(null);
		});
		stream = stream.nonNull();
		stream = stream.sorted();
		stream = stream.distinct();
		stream = stream.map(v -> {
			var name = ZDUtils.generateUserName(v.getAddress(), v.getName());
			return v.toBuilder().name(name).build();
		});
		this.additionalCollaborators = stream.map(v -> {
			return AdditionalCollaborator.builder().email(v.getAddress()).name(v.getName()).build();
		}).toList();
	}

	@JsonProperty("metric_set")
	public MetricSet getMetricSet() {
		return metricSet;
	}

	public void setMetricSet(MetricSet metricSet) {
		this.metricSet = metricSet;
	}

	@Override
	@JsonProperty("created_at")
	public Date getCreatedAt() {
		Date createdAt = super.getCreatedAt();
		if (createdAt == null) {
			MetricSet ms = this.getMetricSet();
			if (ms != null)
				createdAt = ms.getCreatedAt();
		}
		return createdAt;
	}

	public Date getGeneratedTimestampDate() {
		Long ts = this.getGeneratedTimestamp();
		if (ts == null || ts <= 0)
			return null;
		return new Date(ts * 1000);
	}

	@JsonProperty("generated_timestamp")
	public Long getGeneratedTimestamp() {
		return generatedTimestamp;
	}

	public void setGeneratedTimestamp(Long generatedTimestamp) {
		this.generatedTimestamp = generatedTimestamp;
	}

	public static void main(String[] args)  {
		var tv2 = new TicketV2();
		tv2.setAdditionalCollaborators(Map.of("reggie.pierce@gmail.com", ""));

	}

}
