package com.lfp.zendesk.core.model;

import org.zendesk.client.v2.model.events.Event;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.serial.gson.GsonFireBuilderModifier;
import com.lfp.joe.serial.serializer.Serializer;

import io.gsonfire.GsonFireBuilder;
import io.gsonfire.PostProcessor;
import io.gsonfire.TypeSelector;

@Serializer
public class EventSerializer implements GsonFireBuilderModifier {

	@Override
	public GsonFireBuilder apply(GsonFireBuilder gsonFireBuilder) {
		gsonFireBuilder.registerTypeSelector(Event.class, new TypeSelector<Event>() {

			@SuppressWarnings("unchecked")
			@Override
			public Class<? extends Event> getClassForElement(JsonElement readElement) {
				var ct = Serials.Gsons.deserializeClassType(readElement).orElse(null);
				if (ct != null && Event.class.isAssignableFrom(ct))
					return (Class<? extends Event>) ct;
				return null;
			}
		});
		gsonFireBuilder.registerPostProcessor(Event.class, new PostProcessor<Event>() {

			@Override
			public void postDeserialize(Event result, JsonElement src, Gson gson) {}

			@Override
			public void postSerialize(JsonElement result, Event src, Gson gson) {
				if (src != null)
					Serials.Gsons.serializeClassType(result, src.getClass(), false);

			}
		});
		return gsonFireBuilder;
	}

}
