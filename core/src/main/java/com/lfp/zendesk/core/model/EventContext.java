package com.lfp.zendesk.core.model;

import java.io.IOException;
import java.util.Arrays;
import java.util.Objects;
import java.util.Optional;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.zendesk.client.v2.model.Audit;
import org.zendesk.client.v2.model.events.CommentEvent;
import org.zendesk.client.v2.model.events.Event;

import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.net.html.Jsoups;
import com.lfp.joe.utils.Utils;
import com.lfp.mail.core.EmailMessage;

import one.util.streamex.StreamEx;

public class EventContext {

	private final long ticketId;
	private final Audit audit;
	private final Event event;

	private final transient ThrowingSupplier<EmailMessage, IOException> emailMessageGetter;
	private transient Optional<Document> _documentOp;

	public EventContext(long ticketId, Audit audit, Event event,
			ThrowingSupplier<EmailMessage, IOException> emailMessageGetter) {
		this.ticketId = ticketId;
		this.audit = Objects.requireNonNull(audit);
		this.event = Objects.requireNonNull(event);
		this.emailMessageGetter = emailMessageGetter;
	}

	public Optional<EmailMessage> getEmailMessage() throws IOException {
		if (emailMessageGetter == null)
			return Optional.empty();
		return Optional.ofNullable(emailMessageGetter.get());
	}

	public Optional<Document> getDocument() {
		if (_documentOp == null)
			synchronized (this) {
				if (_documentOp == null) {
					var htmlBody = Utils.Types.tryCast(this.event, CommentEvent.class).map(CommentEvent::getHtmlBody)
							.orElse(null);
					_documentOp = Jsoups.tryParse(htmlBody, null, null);
				}
			}
		return _documentOp;
	}

	public StreamEx<String> streamText() {
		return streamText(false);
	}

	public StreamEx<String> streamText(boolean disableHTMLStrip) {
		var commentEvent = Utils.Types.tryCast(this.event, CommentEvent.class).orElse(null);
		if (commentEvent == null)
			return StreamEx.empty();
		StreamEx<String> result = Utils.Lots.stream(commentEvent.getBody());
		if (!disableHTMLStrip) {
			var htmlText = getDocument().map(Element::text).orElse(null);
			result = result.append(Arrays.asList(htmlText));
		}
		result = result.filter(Utils.Strings::isNotBlank);
		return result;
	}

	public long getTicketId() {
		return ticketId;
	}

	public Audit getAudit() {
		return audit;
	}

	public Event getEvent() {
		return event;
	}

}
