
package com.lfp.zendesk.core.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Minutes {

	@SerializedName("calendar")
	@Expose
	private Integer calendar;
	@SerializedName("business")
	@Expose
	private Integer business;

	public Integer getCalendar() {
		return calendar;
	}

	public void setCalendar(Integer calendar) {
		this.calendar = calendar;
	}

	public Minutes withCalendar(Integer calendar) {
		this.calendar = calendar;
		return this;
	}

	public Integer getBusiness() {
		return business;
	}

	public void setBusiness(Integer business) {
		this.business = business;
	}

	public Minutes withBusiness(Integer business) {
		this.business = business;
		return this;
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("calendar", calendar).append("business", business).toString();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder().append(calendar).append(business).toHashCode();
	}

	@Override
	public boolean equals(Object other) {
		if (other == this) {
			return true;
		}
		if ((other instanceof Minutes) == false) {
			return false;
		}
		Minutes rhs = ((Minutes) other);
		return new EqualsBuilder().append(calendar, rhs.calendar).append(business, rhs.business).isEquals();
	}

}
