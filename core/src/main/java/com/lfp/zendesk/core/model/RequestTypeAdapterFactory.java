package com.lfp.zendesk.core.model;

import java.io.IOException;
import java.util.Objects;

import com.lfp.joe.serial.Serials;
import com.lfp.joe.utils.Utils;

import org.zendesk.client.v2.model.Request;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonNull;
import com.google.gson.TypeAdapter;
import com.google.gson.TypeAdapterFactory;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

@SuppressWarnings({ "deprecation", "unchecked" })
public interface RequestTypeAdapterFactory extends TypeAdapterFactory {

	@Override
	default <T> TypeAdapter<T> create(Gson gson, TypeToken<T> typeToken) {
		if (!TypeToken.get(Request.class).isAssignableFrom(typeToken))
			return null;
		return new TypeAdapter<T>() {

			@Override
			public void write(JsonWriter out, T src) throws IOException {
				JsonElement jsonElement;
				if (src == null)
					jsonElement = JsonNull.INSTANCE;
				else {
					byte[] barr = Utils.Functions.unchecked(() -> getObjectMapper().writeValueAsBytes(src));
					jsonElement = Serials.Gsons.getJsonParser()
							.parse(Utils.Bits.bufferedReader(Utils.Bits.from(barr).inputStream()));
				}
				gson.toJson(jsonElement, out);
			}

			@Override
			public T read(JsonReader in) throws IOException {
				var json = Serials.Gsons.getJsonParser().parse(in);
				json = transform(json, typeToken);
				if (json == null || json.isJsonNull())
					return null;
				var bytes = Serials.Gsons.toBytes(json);
				var javaType = getObjectMapper().getTypeFactory().constructType(typeToken.getType());
				var request = (Request) Utils.Functions
						.unchecked(() -> getObjectMapper().readValue(bytes.array(), javaType));
				return (T) request;
			}
		};
	}

	ObjectMapper getObjectMapper();

	private static JsonElement transform(JsonElement json, TypeToken<?> typeToken) {
		if (json == null || json.isJsonNull())
			return json;
		var jo = Serials.Gsons.tryGetAsJsonObject(json).orElse(null);
		if (jo == null || jo.size() != 1)
			return json;
		var key = jo.keySet().iterator().next();
		var classType = Utils.Types.toClass(typeToken.getType());
		while (classType != null && !Objects.equals(Request.class, classType)) {
			if (Utils.Strings.equalsIgnoreCase(classType.getSimpleName(), key))
				return jo.get(key);
			classType = classType.getSuperclass();
		}
		return json;
	}
}
