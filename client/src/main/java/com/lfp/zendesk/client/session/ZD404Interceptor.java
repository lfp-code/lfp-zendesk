package com.lfp.zendesk.client.session;

import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Objects;

import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.utils.Utils;
import com.lfp.zendesk.client.ZDClient;

import okhttp3.Interceptor;
import okhttp3.Response;
import one.util.streamex.StreamEx;

public class ZD404Interceptor implements Interceptor {

	private final ZDClient zdClient;

	public ZD404Interceptor(ZDClient zdClient) {
		this.zdClient = Objects.requireNonNull(zdClient);
	}

	@Override
	public Response intercept(Chain chain) throws IOException {
		var response = chain.proceed(chain.request());
		if (!StatusCodes.matches(response.code(), 301, 302))
			return response;
		var redirectURI = URIs.parse(response.header(Headers.LOCATION)).orElse(null);
		if (redirectURI == null)
			return response;
		boolean is404Redirect = is404Redirect(redirectURI);
		if (!is404Redirect)
			return response;
		response = response.newBuilder().removeHeader(Headers.LOCATION).code(404).build();
		return response;
	}

	private boolean is404Redirect(URI redirectURI) {
		List<URI> uris;
		{
			StreamEx<URI> stream = Utils.Lots.stream(redirectURI);
			var qstream = URIs.streamQueryParameters(redirectURI.getQuery()).filterKeys("return_to"::equals).values();
			stream = stream.append(qstream.map(v -> URIs.parse(v).orElse(null)));
			uris = stream.nonNull().distinct().toList();
		}
		boolean hostMatch = Utils.Lots.stream(uris).anyMatch(uri -> {
			if (Utils.Strings.equals(this.zdClient.getURI().getHost(), uri.getHost()))
				return true;
			return false;
		});
		if (!hostMatch)
			return false;
		for (var uri : uris) {
			boolean foundHC = false;
			String lastSegment = null;
			for (var seg : URIs.streamPathSegments(uri)) {
				if (!foundHC && Utils.Strings.equals(seg, "hc"))
					foundHC = true;
				lastSegment = seg;
			}
			if (foundHC && Utils.Strings.equals(lastSegment, Objects.toString(404)))
				return true;
		}
		return false;
	}

}
