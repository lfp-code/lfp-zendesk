package com.lfp.zendesk.client.tools;

import java.time.ZonedDateTime;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;

import org.threadly.concurrent.SameThreadSubmitterExecutor;

import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.tendis.client.TendisClients;
import com.lfp.joe.core.function.Scrapable;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.zendesk.client.ZDClient;
import com.lfp.zendesk.client.Zendesks;
import com.lfp.zendesk.core.model.TicketV2;

import one.util.streamex.StreamEx;

public class IncrementalExportIteratorCached extends Scrapable.Impl implements Iterator<IncrementalExportResult> {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private final IncrementalExportDayFunctionCached incrementalExportDayFunctionCached;
	private final ZDClient client;
	private final RedissonClientLFP storageClient;
	private final Date startDate;
	private final Iterator<IncrementalExportResult> delegate;

	public IncrementalExportIteratorCached(ZDClient client, Date startDate) {
		this(client, startDate, TendisClients.getDefault());
	}

	public IncrementalExportIteratorCached(ZDClient client, Date startDate, RedissonClientLFP storageClient) {
		this.client = Objects.requireNonNull(client);
		this.storageClient = Objects.requireNonNull(storageClient);
		this.startDate = Optional.ofNullable(startDate).orElse(new Date(0));
		this.incrementalExportDayFunctionCached = new IncrementalExportDayFunctionCached(client, storageClient);
		StreamEx<IncrementalExportResult> delegateStream = Utils.Lots.defer(() -> createDelegateStream());
		this.delegate = delegateStream.iterator();
	}

	protected StreamEx<IncrementalExportResult> createDelegateStream() {
		var dateStream = createDateStream();
		var hasNext = Utils.Lots.hasNext(dateStream);
		if (!hasNext.getKey())
			return StreamEx.empty();
		dateStream = hasNext.getValue();
		var pool = Threads.Pools.centralPool().limit(4);
		var futureStream = dateStream.map(day -> {
			var future = pool.submit(() -> {
				var resultStream = incrementalExportDayFunctionCached.apply(Utils.Times.toDate(day));
				resultStream = resultStream.map(v -> v.filterToDay(Utils.Times.toDate(day)));
				return resultStream.toList();
			});
			return future;
		});
		var resultStream = Threads.Futures.completeStream(futureStream, false, true,
				SameThreadSubmitterExecutor.instance());
		this.onScrap(resultStream::close);
		Set<Long> ticketIds = new HashSet<>();
		StreamEx<IncrementalExportResult> stream = resultStream.nonNull().map(incrementalExports -> {
			var ticketStream = Utils.Lots.flatMapIterables(Utils.Lots.stream(incrementalExports));
			ticketStream = ticketStream.sorted(TicketV2.UPDATED_AT_COMPARATOR);
			ticketStream = ticketStream.filter(v -> v.getId() != null && ticketIds.add(v.getId()));
			var ticketList = ticketStream.toList();
			if (ticketList.isEmpty())
				return null;
			return IncrementalExportResult.builder().tickets(ticketList).build();
		});
		return stream;
	}

	private StreamEx<ZonedDateTime> createDateStream() {
		var dayZero = new IncrementalExportDayZeroSupplier(this.client, this.storageClient).get().orElse(null);
		if (dayZero == null)
			return StreamEx.empty();
		ZonedDateTime lastDay;
		if (this.startDate != null && this.startDate.after(dayZero))
			lastDay = Utils.Times.toDateTime(this.startDate);
		else
			lastDay = Utils.Times.toDateTime(dayZero);
		var start = Utils.Times.startOfDay();
		return Utils.Lots.stream(il -> {
			var dayDate = start.minusDays(il.getIndex());
			if (dayDate.toInstant().toEpochMilli() < lastDay.toInstant().toEpochMilli())
				return il.end();
			return dayDate;
		});
	}

	@Override
	public boolean hasNext() {
		return delegate.hasNext();
	}

	@Override
	public IncrementalExportResult next() {
		return delegate.next();
	}

	public static void main(String[] args) {
		var iter = new IncrementalExportIteratorCached(Zendesks.get(),
				Utils.Times.toDate(Utils.Times.now().minusDays(720)));
		StreamEx<TicketV2> ticketStream = Utils.Lots.flatMapIterables(Utils.Lots.stream(iter));
		int index = -1;
		for (TicketV2 t : ticketStream) {
			index++;
			if (index % 100 == 0)
				System.out.println(index + " - " + t.getUpdatedAt());
		}
		System.out.println("total:" + index);
		System.exit(0);
	}

}