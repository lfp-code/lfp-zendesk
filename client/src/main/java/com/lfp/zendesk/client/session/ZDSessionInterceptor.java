package com.lfp.zendesk.client.session;

import java.io.IOException;
import java.net.URI;
import java.time.Duration;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;

import com.lfp.data.redisson.tools.accessor.SemaphoreAccessor;
import com.lfp.zendesk.client.ZDClient;
import com.lfp.zendesk.client.Zendesks;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.net.cookie.CookieStoreLFP;
import com.lfp.joe.net.html.Jsoups;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.okhttp.cookie.CookieJars;
import com.lfp.joe.okhttp.interceptor.BrowserHeaderInterceptor;
import com.lfp.joe.utils.Utils;

import org.jsoup.nodes.Document;

import de.xn__ho_hia.storage_unit.StorageUnits;
import io.mikael.urlbuilder.UrlBuilder;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class ZDSessionInterceptor implements Interceptor {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 1;
	private static final String SIGNIN_PATH = "/auth/v2/login/signin";
	private static final String SIGNED_IN_PATH = "/auth/v2/login/signed_in";

	private final ZDClient zdClient;

	public ZDSessionInterceptor(ZDClient zdClient) {
		this.zdClient = Objects.requireNonNull(zdClient);
		this.zdClient.getRealm().map(v -> Utils.Strings.trimToNull(v.getPrincipal()))
				.orElseThrow(() -> new IllegalArgumentException("realm principal required"));
		this.zdClient.getRealm().map(v -> Utils.Strings.trimToNull(v.getPassword()))
				.orElseThrow(() -> new IllegalArgumentException("realm password required"));
	}

	@Override
	public Response intercept(Chain chain) throws IOException {
		var request = chain.request();
		var requestURI = request.url().uri();
		if (!Utils.Strings.equalsIgnoreCase(requestURI.getHost(), this.zdClient.getURI().getHost()))
			return chain.proceed(request);
		if (isLoginURI(requestURI))
			return chain.proceed(request);
		var cookieStore = Ok.Clients.getClient(chain.call()).map(v -> Ok.Clients.getCookieStore(v).orElse(null))
				.orElseThrow(() -> new IOException("unable to get cookie store from call"));
		var lock = SemaphoreAccessor.create(1, Duration.ofSeconds(5), THIS_CLASS, VERSION, IPs.getIPAddress(), "lock");
		try {
			return intercept(chain, cookieStore, lock);
		} catch (Exception e) {
			throw Utils.Exceptions.as(e, IOException.class);
		}
	}

	protected Response intercept(Chain chain, CookieStoreLFP cookieStore, SemaphoreAccessor lock) throws Exception {
		URI loginURI;
		{// attempt to proceed
			var response = chain.proceed(chain.request());
			loginURI = getLoginURI(response).orElse(null);
			if (loginURI == null)
				return response;
			response.close();
		}
		if (!lock.isAccessThread())
			return lock.access(() -> intercept(chain, cookieStore, lock));
		login(chain, cookieStore, loginURI);
		// retry
		var response = chain.proceed(chain.request());
		if (getLoginURI(response).isEmpty())
			return response;
		response.close();
		throw Ok.Calls.createException(response, "login page presented post login");
	}

	private void login(Chain chain, CookieStoreLFP cookieStore, URI loginURI) throws IOException {
		Document doc;
		{// get sign in doc
			var rb = new Request.Builder().url(loginURI.toString());
			try (var response = chain.proceed(rb.build())) {
				Ok.Calls.validateStatusCode(response, 2);
				doc = Jsoups.parse(response.body().byteStream(), response.headers().toMultimap(),
						response.request().url().uri());
			}
		}
		{// send credentials
			var loginForm = Jsoups.select(doc, "#login-form").findFirst().orElseThrow(() -> {
				var msg = "could not find login form element";
				if (MachineConfig.isDeveloper())
					msg += Utils.Strings.newLine() + doc.outerHtml();
				throw new IllegalStateException(msg);
			});
			var formData = Jsoups.parseFormData(loginForm);
			formData.set("user[email]", this.zdClient.getRealm().get().getPrincipal());
			formData.set("user[password]", this.zdClient.getRealm().get().getPassword());
			try (var response = chain.proceed(Ok.Calls.toRequest(formData))) {
				Ok.Calls.validateStatusCode(response, 2);
				if (!URIs.pathPrefixMatch(response.request().url().uri().getPath(), SIGNED_IN_PATH))
					throw Ok.Calls.createException(response, "unexpected path. expected:" + SIGNED_IN_PATH);
			}
		}
	}

	private Optional<URI> getLoginURI(Response response) throws IOException {
		if (response == null)
			return Optional.empty();
		if (!Ok.Calls.contentTypeMatches(response, "text", "html", null))
			return Optional.empty();
		return Ok.Calls.peekDocument(response, StorageUnits.megabyte(5).longValue(), doc -> {
			return getLoginURI(doc);
		});
	}

	private Optional<URI> getLoginURI(Document doc) {
		if (doc == null)
			return Optional.empty();
		{
			var uriStream = Jsoups.select(doc, "iframe[src]").map(v -> v.absUrl("src"))
					.map(v -> URIs.parse(v).orElse(null));
			uriStream = uriStream.nonNull().distinct();
			var iframeURIOp = uriStream.filter(this::isLoginURI).findFirst();
			if (iframeURIOp.isPresent())
				return iframeURIOp;
		}
		{
			var dataStream = Jsoups.select(doc, "script[data-brand-id][data-auth-origin][data-auth-domain]").map(v -> {
				Map<String, String> data = new LinkedHashMap<>();
				data.put("auth-origin", v.attr("data-auth-origin"));
				data.put("brand-id", v.attr("data-brand-id"));
				var noBlanks = Utils.Lots.stream(data).values().noneMatch(Utils.Strings::isBlank);
				if (noBlanks)
					return data;
				return null;
			});
			dataStream = dataStream.nonNull();
			var data = dataStream.findFirst().orElse(null);
			if (data != null) {
				var urlb = UrlBuilder.fromUri(this.zdClient.getURI()).withPath(SIGNIN_PATH);
				for (var ent : data.entrySet())
					urlb = urlb.addParameter(ent.getKey(), ent.getValue());
				return Optional.of(urlb.toUri());
			}
		}
		return Optional.empty();
	}

	private boolean isLoginURI(URI uri) {
		if (uri == null)
			return false;
		if (!Utils.Strings.equalsIgnoreCase(uri.getHost(), this.zdClient.getURI().getHost()))
			return false;
		if (!URIs.pathPrefixMatch(uri.getPath(), SIGNIN_PATH))
			return false;
		return true;
	}

	public static void main(String[] args) throws IOException {
		ZDClient zdClient = Zendesks.get();
		var okb = Ok.Clients.get().newBuilder();
		okb.cookieJar(CookieJars.create());
		okb.addNetworkInterceptor(new ZD404Interceptor(zdClient));
		okb.addInterceptor(BrowserHeaderInterceptor.getBrowserDefault());
		okb.addInterceptor(new ZDSessionInterceptor(zdClient));
		var client = okb.build();
		try (var rctx = Ok.Calls
				.execute(client, "https://iplasso.zendesk.com/audits/34983135999/email.html?ticket_id=6314")
				.validateSuccess()) {
			System.out.println(rctx.parseText().getParsedBody());
		}
	}

}
