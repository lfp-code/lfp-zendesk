package com.lfp.zendesk.client.config;

import java.net.URI;
import java.time.Duration;

import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.core.properties.code.PrintOptions;
import com.lfp.joe.core.properties.converter.URIConverter;
import com.lfp.joe.properties.converter.DurationConverter;

import org.aeonbits.owner.Config;

public interface ZendeskConfig extends Config {

	@ConverterClass(URIConverter.class)
	URI uri();

	String username();

	String password();

	boolean highVolumeAddOnEnabled();

	URI proxyURI();

	@DefaultValue("3")
	int serverErrorRetryCount();
	
	@DefaultValue("3 sec")
	@ConverterClass(DurationConverter.class)
	Duration serverErrorRetryDelay();

	public static void main(String[] args) {
		Configs.printProperties(PrintOptions.json());
		Configs.printProperties(PrintOptions.jsonBuilder().withIncludeValues(true).build());
	}

}
