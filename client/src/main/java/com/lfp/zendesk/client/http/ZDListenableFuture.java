package com.lfp.zendesk.client.http;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

import org.asynchttpclient.ListenableFuture;
import org.threadly.concurrent.future.SettableListenableFuture;

import com.lfp.joe.threads.Threads;

public abstract class ZDListenableFuture<V> extends SettableListenableFuture<V> implements ListenableFuture<V> {

	public ZDListenableFuture() {
		super(false);
	}

	@Override
	public ListenableFuture<V> addListener(Runnable listener, Executor exec) {
		super.listener(listener, exec);
		return this;
	}

	@Override
	public CompletableFuture<V> toCompletableFuture() {
		CompletableFuture<V> completableFuture = new CompletableFuture<>();
		Threads.Futures.forwardCompletion(completableFuture, this);
		Threads.Futures.forwardCompletion(this, completableFuture);
		return completableFuture;
	}

}
