package com.lfp.zendesk.client.tools;

import java.io.IOException;
import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import javax.annotation.Generated;

import org.redisson.RedissonLock;
import org.redisson.api.RList;
import org.redisson.api.RReadWriteLock;
import org.threadly.concurrent.future.ListenableFuture;

import com.github.throwable.beanref.BeanRef;
import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.data.redisson.client.codec.GsonCodec;
import com.lfp.data.tendis.client.TendisClients;
import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.ImmutableFutures.ScheduleWhileRequest;
import com.lfp.joe.utils.Utils;
import com.lfp.zendesk.client.ZDClient;
import com.lfp.zendesk.client.Zendesks;
import com.lfp.zendesk.core.model.TicketV2;

import one.util.streamex.StreamEx;

public class IncrementalExportDayFunctionCached extends IncrementalExportDayFunction {
	private static final Class<?> THIS_CLASS = new Object() {}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 3;
	public static final Duration EXPIRE_AFTER_WRITE_DEFAULT = Duration.ofDays(14);
	public static final Duration EXPIRE_AFTER_ACCESS_DEFAULT = Duration.ofDays(14);
	public static final Duration TODAY_REFRESH_DEFAULT = Duration.ofSeconds(5);
	public static final Duration LOCK_LEASE_TIME = Duration.ofSeconds(5);
	private final RedissonClientLFP storageClient;
	private final GsonCodec gsonCodec;

	public IncrementalExportDayFunctionCached(ZDClient client) {
		this(client, TendisClients.getDefault());
	}

	public IncrementalExportDayFunctionCached(ZDClient client, RedissonClientLFP storageClient) {
		super(client);
		this.storageClient = Objects.requireNonNull(storageClient);
		this.gsonCodec = new GsonCodec(client.getGson());
	}

	@Override
	public StreamEx<IncrementalExportResult> apply(Date startDate) {
		return apply(startDate, null);
	}

	public StreamEx<IncrementalExportResult> apply(Date startDate, Options options) {
		if (options == null)
			options = Options.builder().build();
		if (startDate == null || startDate.after(new Date()) || startDate.before(new Date(0)))
			return StreamEx.empty();
		var startTime = Utils.Times.startOfDay(Utils.Times.toDateTime(startDate));
		List<IncrementalExportResult> results;
		try {
			results = applyInternal(startTime, options);
		} catch (IOException e) {
			throw Utils.Exceptions.asRuntimeException(e);
		}
		return Utils.Lots.stream(results).filter(v -> !v.isEmpty());
	}

	protected List<IncrementalExportResult> applyInternal(ZonedDateTime startTime, Options options) throws IOException {
		RList<IncrementalExportResult> list = this.storageClient.getList(generateKey(startTime, "list"),
				this.gsonCodec);
		var rwLock = RedissonClients.getDefault().getReadWriteLock(generateKey(startTime, "rw-lock"));
		List<IncrementalExportResult> results = new ArrayList<>();
		var updateStrategy = readAccess(rwLock, () -> {
			var us = getUpdateStrategy(startTime, options, list);
			if (UpdateStrategy.NONE.equals(us)) {
				updateExpiration(startTime, options, list);
				addAll(list, results);
			} else if (UpdateStrategy.UPDATE.equals(us))
				addAll(list, results);
			return us;
		});
		if (UpdateStrategy.NONE.equals(updateStrategy))
			return results;
		boolean disableFreshDateTruncate = UpdateStrategy.UPDATE.equals(updateStrategy);
		Date freshRecordDate;
		{
			var dStream = Utils.Lots.stream(results).map(v -> v.getLatestUpdatedAt().orElse(null));
			dStream = dStream.nonNull();
			dStream = dStream.sortedBy(v -> -1 * v.getTime());
			freshRecordDate = dStream.findFirst().orElse(Utils.Times.toDate(startTime));
		}
		var freshRecords = Utils.Lots.asCollection(super.apply(freshRecordDate, disableFreshDateTruncate));
		boolean write;
		if (UpdateStrategy.FRESH.equals(updateStrategy))
			write = true;
		else
			write = newTicketFound(results, freshRecords);
		if (!write)
			return results;
		return writeAccess(rwLock, () -> {
			var recordStream = Utils.Lots.stream(results).append(Utils.Lots.stream(freshRecords));
			recordStream = recordStream.nonNull();
			var ticketStream = Utils.Lots.flatMapIterables(recordStream);
			// require id
			ticketStream = ticketStream.filter(v -> v.getId() != null);
			// latest first
			ticketStream = ticketStream.sorted(TicketV2.UPDATED_AT_COMPARATOR);
			// distinct id
			ticketStream = ticketStream.distinct(v -> v.getId());
			// reverse
			var tickets = Utils.Lots.reverseView(ticketStream.toList());
			var resultStream = Utils.Lots.buffer(tickets, b -> b.size() >= 1000).map(v -> {
				return IncrementalExportResult.builder().tickets(v).build();
			});
			results.clear();
			results.addAll(resultStream.toList());
			writeList(startTime, options, list, results);
			return results;
		});
	}

	protected boolean newTicketFound(List<IncrementalExportResult> results,
			Iterable<IncrementalExportResult> freshRecords) {
		for (var freshRecord : freshRecords) {
			for (var ticket : freshRecord) {
				for (var result : results) {
					if (!result.contains(ticket))
						return true;
				}
			}
		}
		return false;
	}

	protected UpdateStrategy getUpdateStrategy(ZonedDateTime startTime, Options options,
			RList<IncrementalExportResult> list) {
		var size = list.size();
		if (size == 0)
			return UpdateStrategy.FRESH;
		IncrementalExportResult lastRecord = list.get(size - 1);
		if (!lastRecord.isEmpty())
			return UpdateStrategy.FRESH;
		if (Utils.Times.isToday(startTime) && options.todayRefresh != null) {
			long elapsed = System.currentTimeMillis() - lastRecord.getCreatedAt().getTime();
			if (elapsed > options.todayRefresh.toMillis())
				return UpdateStrategy.UPDATE;
		}
		return UpdateStrategy.NONE;
	}

	protected void updateExpiration(ZonedDateTime startTime, Options options, RList<IncrementalExportResult> list) {
		var expiration = getExpiration(startTime, options.expireAfterAccess);
		if (expiration == null)
			return;
		list.expireAsync(expiration.toMillis(), TimeUnit.MILLISECONDS);
	}

	protected void writeList(ZonedDateTime startTime, Options options, RList<IncrementalExportResult> list,
			Iterable<IncrementalExportResult> records) {
		var expiration = getExpiration(startTime, options.expireAfterWrite);
		StreamEx<IncrementalExportResult> writeRecordsStream;
		{
			writeRecordsStream = StreamEx.empty();
			if (expiration == null || expiration.toMillis() > 0) {
				var append = Utils.Lots.stream(records).map(v -> v.filterToDay(Utils.Times.toDate(startTime)));
				writeRecordsStream = writeRecordsStream.append(append);
			}
			writeRecordsStream = writeRecordsStream.append(IncrementalExportResult.builder().build());
		}
		Runnable extendExpiration = () -> {
			if (expiration != null)
				list.expire(expiration.toMillis(), TimeUnit.MILLISECONDS);
		};
		var writeRecordsIterator = writeRecordsStream.iterator();
		for (int i = 0; writeRecordsIterator.hasNext(); i++) {
			var next = writeRecordsIterator.next();
			if (i == 0) {
				list.clear();
				extendExpiration.run();
			}
			list.add(next);
			extendExpiration.run();
		}
	}

	protected Duration getExpiration(ZonedDateTime startTime, Duration requestedExpiration) {
		var startOfDay = Utils.Times.startOfDay();
		boolean isToday = !startTime.isBefore(startOfDay);
		if (!isToday)
			return requestedExpiration;
		var startOfTom = startOfDay.plusDays(1);
		var ttl = startOfTom.toInstant().toEpochMilli() - System.currentTimeMillis();
		// allow for a little drift
		ttl = ttl - Duration.ofSeconds(10).toMillis();
		ttl = Math.max(0, ttl);
		return Duration.ofMillis(ttl);
	}

	private String generateKey(ZonedDateTime dateTime, Object... append) {
		StreamEx<Object> stream = Utils.Lots.stream(THIS_CLASS, VERSION, this.getClient().getURI());
		stream = stream.append(Utils.Lots.stream(Utils.Times.startOfDay(dateTime)));
		stream = stream.append(Utils.Lots.stream(append));
		return RedisUtils.generateKey(stream.toArray());
	}

	private static void addAll(RList<IncrementalExportResult> list, List<IncrementalExportResult> results) {
		for (int i = 0; i < list.size(); i++)
			results.add(list.get(i));
	}

	private static <X, T extends Throwable> X readAccess(RReadWriteLock rwLock, ThrowingSupplier<X, T> accessor)
			throws T {
		return lockAccess((RedissonLock) rwLock.readLock(), accessor);
	}

	private static <X, T extends Throwable> X writeAccess(RReadWriteLock rwLock, ThrowingSupplier<X, T> accessor)
			throws T {
		return lockAccess((RedissonLock) rwLock.writeLock(), accessor);
	}

	private static <X, T extends Throwable> X lockAccess(RedissonLock lock, ThrowingSupplier<X, T> accessor) throws T {
		lock.lock(LOCK_LEASE_TIME.toMillis(), TimeUnit.MILLISECONDS);
		ListenableFuture<Nada> future = null;
		try {
			future = ScheduleWhileRequest.builder(LOCK_LEASE_TIME.dividedBy(2), () -> {
				try {
					lock.expire(LOCK_LEASE_TIME.toMillis(), TimeUnit.MILLISECONDS);
				} catch (Exception e) {
					var cancelException = Utils.Exceptions.streamCauses(e).filter(Utils.Exceptions::isCancelException)
							.findFirst().isPresent();
					if (!cancelException) {
						String msg = "lock extension failed";
						if (MachineConfig.isDeveloper())
							logger.warn(msg, e);
						else
							logger.warn(msg + ": " + e.getMessage());
					}
				}
			}, () -> lock.isExists()).initialDelay(LOCK_LEASE_TIME.dividedBy(2)).build().schedule();
			return accessor.get();
		} finally {
			if (future != null)
				future.cancel(true);
			lock.unlock();
		}
	}

	public static class Options {

		public final Duration expireAfterWrite;
		public final Duration expireAfterAccess;
		public final Duration todayRefresh;

		@Generated("SparkTools")
		private Options(Builder builder) {
			this.expireAfterWrite = builder.expireAfterWrite;
			this.expireAfterAccess = builder.expireAfterAccess;
			this.todayRefresh = builder.todayRefresh;
		}

		/**
		 * Creates builder to build {@link Options}.
		 * 
		 * @return created builder
		 */
		@Generated("SparkTools")
		public static Builder builder() {
			return new Builder();
		}

		/**
		 * Builder to build {@link Options}.
		 */
		@Generated("SparkTools")
		public static final class Builder {
			private Duration expireAfterWrite = EXPIRE_AFTER_WRITE_DEFAULT;
			private Duration expireAfterAccess = EXPIRE_AFTER_ACCESS_DEFAULT;
			private Duration todayRefresh = TODAY_REFRESH_DEFAULT;

			private Builder() {}

			public Builder withExpireAfterWrite(Duration expireAfterWrite) {
				this.expireAfterWrite = expireAfterWrite;
				return this;
			}

			public Builder withExpireAfterAccess(Duration expireAfterAccess) {
				this.expireAfterAccess = expireAfterAccess;
				return this;
			}

			public Builder withTodayRefresh(Duration todayRefresh) {
				this.todayRefresh = todayRefresh;
				return this;
			}

			public Options build() {
				return new Options(this);
			}
		}

	}

	private static enum UpdateStrategy {
		NONE, FRESH, UPDATE;
	}

	public static void main(String[] args) {
		Function<IncrementalExportResult, String> summaryGetter = r -> {
			var printRecord = r.toBuilder().tickets(List.of()).build();
			var jo = Zendesks.get().getGson().toJsonTree(printRecord).getAsJsonObject();
			var meta = IncrementalExportResult.meta();
			jo.remove(meta.tickets().name());
			jo.addProperty(meta.tickets().name(), String.format("[%s]", r.size()));
			var latestUpdatedAt = r.getLatestUpdatedAt().orElse(null);
			if (latestUpdatedAt != null)
				jo.addProperty(BeanRef.$(IncrementalExportResult::getLatestUpdatedAt).getPath(),
						latestUpdatedAt.toString());
			return Serials.Gsons.getPretty().toJson(jo);
		};
		var startDate = Utils.Times.toDate(Utils.Times.startOfDay().minusDays(721));
		var reader = new IncrementalExportDayFunctionCached(Zendesks.get());
		var records = reader.apply(startDate);
		for (var record : records) {
			System.out.println(summaryGetter.apply(record));
		}
		System.exit(0);
	}
}
