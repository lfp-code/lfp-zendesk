package com.lfp.zendesk.client;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Objects;
import java.util.concurrent.Callable;

import com.lfp.zendesk.client.config.ZendeskConfig;
import com.lfp.zendesk.client.http.ZDAsyncHttpClient;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.utils.Utils;

import org.zendesk.client.v2.Zendesk;

import javassist.util.proxy.MethodHandler;

@SuppressWarnings("unchecked")
public class Zendesks {

	private static final MemoizedSupplier<Zendesk> DEFAULT_INSTANCE = Utils.Functions.memoize(() -> {
		return create(Configs.get(ZendeskConfig.class));
	});

	public static <Z extends Zendesk & ZDClient> Z create(ZendeskConfig zendeskConfig) {
		Objects.requireNonNull(zendeskConfig);
		Callable<Zendesk> zendeskCallable = () -> {
			var zdb = new Zendesk.Builder(zendeskConfig.uri().toString());
			zdb.setUsername(zendeskConfig.username());
			zdb.setPassword(zendeskConfig.password());
			zdb.setClient(new ZDAsyncHttpClient(zendeskConfig));
			return zdb.build();
		};
		Z result = create(zendeskCallable);
		return result;
	}

	public static <Z extends Zendesk & ZDClient> Z create(Zendesk zendesk) {
		Objects.requireNonNull(zendesk);
		if (zendesk instanceof ZDClient)
			return (Z) zendesk;
		return create(() -> zendesk);
	}

	public static <Z extends Zendesk & ZDClient> Z create(Callable<Zendesk> delegateCallable) {
		return Utils.Functions.unchecked(() -> createInternal(delegateCallable));
	}

	protected static <Z extends Zendesk & ZDClient> Z createInternal(Callable<Zendesk> delegateCallable)
			throws NoSuchMethodException, IllegalArgumentException, InstantiationException, IllegalAccessException,
			InvocationTargetException {
		var zendeskExt = new ZDClient.Impl(delegateCallable);
		var factory = JavaCode.Proxies.createFactory(Zendesk.class, ZDClient.class);
		return (Z) factory.create(new Class[0], new Object[0], new MethodHandler() {

			@Override
			public Object invoke(Object self, Method thisMethod, Method proceed, Object[] args) throws Throwable {
				if (ZDClient.class.equals(thisMethod.getDeclaringClass()))
					return thisMethod.invoke(zendeskExt, args);
				return thisMethod.invoke(zendeskExt.getZendesk(), args);
			}
		});
	}

	public static <Z extends Zendesk & ZDClient> Z get() {
		return (Z) DEFAULT_INSTANCE.get();
	}

	public static void main(String[] args) {
		var ticket = Zendesks.get().getTicket(514774);
		System.out.println(ticket.getSubject());
		System.out.println(Zendesks.get().getClient().getClientStats());
		// Zendesks.get().close();
		System.exit(0);
	}

}
