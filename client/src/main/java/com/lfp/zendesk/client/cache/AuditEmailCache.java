package com.lfp.zendesk.client.cache;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.Duration;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Predicate;

import org.apache.commons.lang3.Validate;
import org.redisson.api.RBucket;

import com.github.benmanes.caffeine.cache.LoadingCache;
import com.google.common.reflect.TypeToken;
import com.lfp.data.minio.MinioClients;
import com.lfp.data.minio.MinioUtils;
import com.lfp.data.redisson.tools.accessor.SemaphoreAccessor;
import com.lfp.joe.cache.Caches;
import com.lfp.joe.cache.StatValue;
import com.lfp.joe.core.function.Nada;
import com.lfp.joe.net.email.JavaMails;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.utils.Utils;
import com.lfp.mail.core.EmailMessage;
import com.lfp.mail.core.EmailMessages;
import com.lfp.zendesk.client.ZDClient;

import io.mikael.urlbuilder.UrlBuilder;
import io.minio.GetObjectArgs;
import io.minio.MakeBucketArgs;
import io.minio.PutObjectArgs;
import io.minio.errors.ErrorResponseException;
import io.minio.errors.InsufficientDataException;
import io.minio.errors.InternalException;
import io.minio.errors.InvalidResponseException;
import io.minio.errors.ServerException;
import io.minio.errors.XmlParserException;
import one.util.streamex.StreamEx;

public class AuditEmailCache {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 0;
	private static final int CACHE_TTL_DAYS = 7;
	@SuppressWarnings("serial")
	private static final TypeToken<StatValue<EmailMessage>> TT = new TypeToken<StatValue<EmailMessage>>() {
	};
	private static final LoadingCache<Nada, String> BUCKET_NAME_CACHE = Caches
			.newCaffeineBuilder(-1, Duration.ofMinutes(1), null).build(nil -> {
				var minioClient = MinioClients.getDefault();
				var bucketName = MinioUtils.generateBucketName(THIS_CLASS, "audit", "email", VERSION, CACHE_TTL_DAYS);
				boolean created = minioClient.tryMakeBucket(MakeBucketArgs.builder().bucket(bucketName).build());
				if (!minioClient.trySetBucketExpiration(bucketName, CACHE_TTL_DAYS) && created)
					logger.warn("set bucket expiration failed:{}", bucketName);
				return bucketName;
			});

	private final ZDClient client;

	public AuditEmailCache(ZDClient client) {
		super();
		this.client = Objects.requireNonNull(client);
	}

	public void put(long ticketId, long auditId, EmailMessage emailMessage) throws IOException {
		var bucketName = BUCKET_NAME_CACHE.get(Nada.get());
		var objectName = getObjectName(ticketId, auditId);
		try {
			MinioClients.getDefault().putJsonObject(PutObjectArgs.builder().bucket(bucketName).object(objectName),
					StatValue.build(emailMessage));
		} catch (InvalidKeyException | ErrorResponseException | InsufficientDataException | InternalException
				| InvalidResponseException | NoSuchAlgorithmException | ServerException | XmlParserException
				| IllegalArgumentException e) {
			throw Utils.Exceptions.as(e, IOException.class);
		}
	}

	public Optional<EmailMessage> get(long ticketId, long auditId) throws IOException {
		return get(ticketId, auditId, false);
	}

	public Optional<EmailMessage> get(long ticketId, long auditId, boolean refreshCache) throws IOException {
		var lock = SemaphoreAccessor.create(1, Duration.ofSeconds(5), "lock", keyParts(ticketId, auditId).toArray());
		try {
			return get(ticketId, auditId, refreshCache ? new Date() : null, lock);
		} catch (Exception e) {
			throw Utils.Exceptions.as(e, IOException.class);
		}
	}

	protected Optional<EmailMessage> get(long ticketId, long auditId, Date refreshDate, SemaphoreAccessor lock)
			throws Exception {
		var bucketName = BUCKET_NAME_CACHE.get(Nada.get());
		var objectName = getObjectName(ticketId, auditId);
		StatValue<EmailMessage> sv;
		if (refreshDate != null && !lock.isAccessThread())
			sv = null;
		else
			sv = MinioClients.getDefault()
					.tryGetJsonObject(GetObjectArgs.builder().bucket(bucketName).object(objectName).build(), TT)
					.orElse(null);
		if (refreshDate != null && sv != null && refreshDate.after(sv.getCreatedAt()))
			sv = null;
		if (sv != null)
			return Optional.ofNullable(sv.getValue());
		if (!lock.isAccessThread())
			return lock.access(() -> get(ticketId, auditId, refreshDate, lock));
		var emailMessage = getEmailMessage(ticketId, auditId);
		put(ticketId, auditId, emailMessage);
		return Optional.ofNullable(emailMessage);
	}

	private EmailMessage getEmailMessage(long ticketId, long auditId) throws IOException {
		JavaMails.init();
		var emlURI = UrlBuilder.fromUri(this.client.getURI()).withPath(String.format("/audits/%s/email.eml", auditId))
				.addParameter("ticket_id", ticketId + "").toUri();
		var rb = new okhttp3.Request.Builder().url(emlURI.toString());
		var client = this.client.getWebClient();
		try (var response = client.newCall(rb.build()).execute()) {
			Ok.Calls.validateStatusCode(response, 2, 404);
			if (!response.isSuccessful())
				return null;
			return EmailMessages.parseEmailMessage(response.body().byteStream());
		}
	}

	private StreamEx<Object> keyParts(long ticketId, long auditId, Object... append) {
		StreamEx<Object> stream = Utils.Lots.stream(THIS_CLASS, VERSION, this.client.getURI(), ticketId, auditId);
		return stream.append(Utils.Lots.stream(append));
	}

	private static String getObjectName(long ticketId, long auditId) {
		validateIds(ticketId, auditId);
		return String.format("%s.%s.json", ticketId, auditId);
	}

	private static void validateIds(long ticketId, long auditId) {
		Predicate<Long> isValid = v -> v != null && v >= 0;
		Validate.isTrue(isValid.test(ticketId), "invalid ticketId:%s", ticketId);
		Validate.isTrue(isValid.test(auditId), "invalid auditId:%s", auditId);
	}

}
