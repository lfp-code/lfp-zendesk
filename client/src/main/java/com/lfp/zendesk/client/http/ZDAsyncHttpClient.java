package com.lfp.zendesk.client.http;

import java.time.Duration;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeoutException;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.BiConsumer;

import com.lfp.joe.core.config.MachineConfig;
import com.lfp.joe.net.http.headers.HeaderMap;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.proxy.Proxy;
import com.lfp.joe.utils.Utils;
import com.lfp.zendesk.client.config.ZendeskConfig;
import com.lfp.zendesk.core.ZDUtils;

import org.asynchttpclient.AsyncHandler;
import org.asynchttpclient.DefaultAsyncHttpClient;
import org.asynchttpclient.DefaultAsyncHttpClientConfig;
import org.asynchttpclient.ListenableFuture;
import org.asynchttpclient.Realm;
import org.asynchttpclient.Request;
import org.asynchttpclient.config.AsyncHttpClientConfigDefaults;
import org.asynchttpclient.filter.FilterContext;
import org.asynchttpclient.filter.FilterException;
import org.asynchttpclient.filter.ResponseFilter;
import org.asynchttpclient.proxy.ProxyServer;
import org.asynchttpclient.proxy.ProxyType;
import org.threadly.concurrent.SameThreadSubmitterExecutor;

import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslContextBuilder;
import io.netty.handler.ssl.util.InsecureTrustManagerFactory;

public class ZDAsyncHttpClient extends DefaultAsyncHttpClient {

	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String X_RATE_LIMIT_REMAINING_HEADER = "X-Rate-Limit-Remaining";
	private static final int TIMEOUT_RETRY = 3;
	private static final Duration CONNECT_TIMEOUT = Duration.ofMinutes(1);
	private static final Duration READ_TIMEOUT = Duration.ofMinutes(1);
	private static final Duration REQUEST_TIMEOUT = Duration.ofMinutes(1);

	public ZDAsyncHttpClient(ZendeskConfig zendeskConfig) {
		super(createAsyncHttpClientConfigBuilder(zendeskConfig).build());
	}

	@Override
	public <T> ListenableFuture<T> executeRequest(Request request, AsyncHandler<T> handler) {
		if (true)
			return super.executeRequest(request, handler);
		var timeoutRetriesRemaining = new AtomicInteger(TIMEOUT_RETRY);
		ZDListenableFuture<T> result = new ZDListenableFuture<T>() {

			private ListenableFuture<T> delegate;
			{
				Runnable executeTask = new Runnable() {

					@Override
					public void run() {
						synchronized (timeoutRetriesRemaining) {
							var future = ZDAsyncHttpClient.super.executeRequest(request, handler);
							addListener(future, (v, t) -> {
								if (timeoutRetriesRemaining.getAndDecrement() > 0 && isTimeout(t)) {
									this.run();
									return;
								}
								if (t != null)
									setFailure(t);
								else
									setResult(v);
							});
							listener(() -> {
								future.cancel(true);
							});
							delegate = future;
						}
					}

				};
				executeTask.run();
			}

			@Override
			public void done() {
				synchronized (timeoutRetriesRemaining) {
					timeoutRetriesRemaining.set(0);
					this.delegate.done();
				}

			}

			@Override
			public void abort(Throwable t) {
				synchronized (timeoutRetriesRemaining) {
					timeoutRetriesRemaining.set(0);
					this.delegate.abort(t);
				}
			}

			@Override
			public void touch() {
				synchronized (timeoutRetriesRemaining) {
					this.delegate.touch();
				}
			}

			private void addListener(ListenableFuture<T> future, BiConsumer<T, Throwable> listener) {
				future.addListener(() -> {
					try {
						listener.accept(future.get(), null);
					} catch (Throwable t) {
						listener.accept(null, t);
					}
				}, SameThreadSubmitterExecutor.instance());

			}

			private boolean isTimeout(Throwable error) {
				if (error == null)
					return false;
				var stream = Utils.Exceptions.streamCauses(error);
				return stream.anyMatch(v -> {
					if (v instanceof TimeoutException)
						return true;
					if (Utils.Strings.containsIgnoreCase(v.getClass().getName(), "timeout"))
						return true;
					return false;
				});
			}
		};
		return result;
	}

	private static DefaultAsyncHttpClientConfig.Builder createAsyncHttpClientConfigBuilder(
			ZendeskConfig zendeskConfig) {
		Objects.requireNonNull(zendeskConfig);
		DefaultAsyncHttpClientConfig.Builder clientConfigBuilder = new DefaultAsyncHttpClientConfig.Builder();
		clientConfigBuilder.setConnectTimeout((int) CONNECT_TIMEOUT.toMillis());
		clientConfigBuilder.setReadTimeout((int) READ_TIMEOUT.toMillis());
		clientConfigBuilder.setRequestTimeout((int) REQUEST_TIMEOUT.toMillis());
		clientConfigBuilder.addRequestFilter(new ZendeskRateLimiter(zendeskConfig.highVolumeAddOnEnabled()));
		clientConfigBuilder.addResponseFilter(createFailureLoggingResponseFilter());
		clientConfigBuilder.addResponseFilter(createRateLimitRemainingLoggingResponseFilter());
		var proxyURI = zendeskConfig.proxyURI();
		if (proxyURI != null) {
			var proxy = Proxy.builder(proxyURI).build();
			ProxyType proxyType;
			if (Proxy.Type.SOCKS_5.equals(proxy.getType()))
				proxyType = ProxyType.SOCKS_V5;
			else
				proxyType = ProxyType.HTTP;
			Realm proxyRealm;
			if (!proxy.isAuthenticationEnabled())
				proxyRealm = null;
			else {
				var rb = new Realm.Builder(proxy.getUsername().orElse(""), proxy.getPassword().orElse(""));
				rb.setScheme(Realm.AuthScheme.BASIC);
				rb.setUsePreemptiveAuth(true);
				proxyRealm = rb.build();
			}
			if (MachineConfig.isDeveloper() && URIs.isLocalhost(proxy.getHostname())) {
				SslContext sslContext = Utils.Functions.unchecked(() -> {
					return SslContextBuilder.forClient().trustManager(InsecureTrustManagerFactory.INSTANCE).build();
				});
				clientConfigBuilder.setSslContext(sslContext);
			}
			clientConfigBuilder.setProxyServer(new ProxyServer(proxy.getHostname(), proxy.getPort(), proxy.getPort(),
					proxyRealm, null, proxyType));
		}
		return clientConfigBuilder;
	}

	private static ResponseFilter createFailureLoggingResponseFilter() {
		return new ResponseFilter() {

			@Override
			public <T> FilterContext<T> filter(FilterContext<T> ctx) throws FilterException {
				var responseStatus = ctx.getResponseStatus();
				var statusCode = responseStatus.getStatusCode();
				boolean success = statusCode / 100 == 2 || statusCode / 100 == 3 || statusCode == 404;
				if (!success)
					logger.error("request failed. FilterContext summary:{}", getSummary(ctx));
				return ctx;
			}

		};
	}

	private static ResponseFilter createRateLimitRemainingLoggingResponseFilter() {
		return new ResponseFilter() {

			@Override
			public <T> FilterContext<T> filter(FilterContext<T> ctx) throws FilterException {
				if (MachineConfig.isDeveloper()) {
					var headerMap = HeaderMap.of(ctx.getResponseHeaders());
					var rateLimitRemainingValue = headerMap.firstValue(X_RATE_LIMIT_REMAINING_HEADER, true)
							.orElse(null);
					if (rateLimitRemainingValue != null) {
						var path = URIs.parse(ctx.getRequest().getUrl()).map(v -> URIs.normalizePath(v)).orElse(null);
						logger.info(X_RATE_LIMIT_REMAINING_HEADER + ":{} path:{}", rateLimitRemainingValue, path);
					}
				}
				return ctx;
			}

		};
	}

	private static <T> String getSummary(FilterContext<T> ctx) {
		return ZDUtils.getSummary(ctx.getRequest(),
				Optional.ofNullable(ctx.getResponseStatus()).map(v -> v.getStatusCode()).orElse(null),
				ctx.getResponseHeaders());
	}

	public static void main(String[] args) {
		System.out.println(AsyncHttpClientConfigDefaults.defaultMaxRequestRetry());
	}
}
