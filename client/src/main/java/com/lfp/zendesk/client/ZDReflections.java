package com.lfp.zendesk.client;

import com.lfp.joe.reflections.JavaCode;
import com.lfp.joe.reflections.reflection.FieldAccessor;

import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.Realm;
import org.zendesk.client.v2.Zendesk;

import com.fasterxml.jackson.databind.ObjectMapper;

@SuppressWarnings("unchecked")
public class ZDReflections {

	public static final FieldAccessor<Zendesk, AsyncHttpClient> Zendesk_client_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(Zendesk.class, true, v -> "client".equals(v.getName()),
					v -> AsyncHttpClient.class.isAssignableFrom(v.getType()));

	public static final FieldAccessor<Zendesk, Realm> Zendesk_realm_FA = JavaCode.Reflections.getFieldAccessorUnchecked(
			Zendesk.class, true, v -> "realm".equals(v.getName()), v -> Realm.class.isAssignableFrom(v.getType()));

	public static final FieldAccessor<Zendesk, String> Zendesk_oauthToken_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(Zendesk.class, true, v -> "oauthToken".equals(v.getName()),
					v -> String.class.isAssignableFrom(v.getType()));

	public static final FieldAccessor<Zendesk, String> Zendesk_url_FA = JavaCode.Reflections.getFieldAccessorUnchecked(
			Zendesk.class, true, v -> "url".equals(v.getName()), v -> String.class.isAssignableFrom(v.getType()));

	public static final FieldAccessor<Zendesk, ObjectMapper> Zendesk_mapper_FA = JavaCode.Reflections
			.getFieldAccessorUnchecked(Zendesk.class, true, v -> "mapper".equals(v.getName()),
					v -> ObjectMapper.class.isAssignableFrom(v.getType()));

}
