package com.lfp.zendesk.client.http;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Objects;

import com.lfp.joe.utils.Utils;

public class KeepOpenInputStream extends InputStream {

	public static KeepOpenInputStream create(InputStream inputStream) {
		return Utils.Types.tryCast(inputStream, KeepOpenInputStream.class).orElseGet(() -> {
			return new KeepOpenInputStream(inputStream);
		});
	}

	private final InputStream delegate;

	protected KeepOpenInputStream(InputStream delegate) {
		super();
		this.delegate = Objects.requireNonNull(delegate);
	}

	public InputStream getDelegate() {
		return this.delegate;
	}

	@Override
	public void close() throws IOException {
		// noop
	}

	@Override
	public void reset() throws IOException {
		// noop
	}

	@Override
	public int hashCode() {
		return delegate.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		return delegate.equals(obj);
	}

	@Override
	public int read() throws IOException {
		return delegate.read();
	}

	@Override
	public int read(byte[] b) throws IOException {
		return delegate.read(b);
	}

	@Override
	public int read(byte[] b, int off, int len) throws IOException {
		return delegate.read(b, off, len);
	}

	@Override
	public String toString() {
		return delegate.toString();
	}

	@Override
	public byte[] readAllBytes() throws IOException {
		return delegate.readAllBytes();
	}

	@Override
	public byte[] readNBytes(int len) throws IOException {
		return delegate.readNBytes(len);
	}

	@Override
	public int readNBytes(byte[] b, int off, int len) throws IOException {
		return delegate.readNBytes(b, off, len);
	}

	@Override
	public long skip(long n) throws IOException {
		return delegate.skip(n);
	}

	@Override
	public int available() throws IOException {
		return delegate.available();
	}

	@Override
	public void mark(int readlimit) {
		delegate.mark(readlimit);
	}

	@Override
	public boolean markSupported() {
		return delegate.markSupported();
	}

	@Override
	public long transferTo(OutputStream out) throws IOException {
		return delegate.transferTo(out);
	}

}
