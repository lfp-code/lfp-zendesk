package com.lfp.zendesk.client.tools;

import com.lfp.joe.core.lot.Lot; import com.lfp.joe.core.lot.AbstractLot; import java.io.IOException;
import java.net.URI;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

import com.lfp.zendesk.client.ZDClient;
import com.lfp.zendesk.client.Zendesks;
import com.lfp.zendesk.core.model.TicketV2;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.lot.AbstractLot;

import org.apache.commons.lang3.Validate;
import org.asynchttpclient.RequestBuilder;

import com.google.common.reflect.TypeToken;

import io.mikael.urlbuilder.UrlBuilder;
import one.util.streamex.StreamEx;

@SuppressWarnings("serial")
public class IncrementalExportIterator extends AbstractLot.Indexed<IncrementalExportResult> {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final String START_TIME_PARAM = "start_time";
	private static final String CURSOR_PARAM = "cursor";
	private static final String PAGE_SIZE_PARAM = "page_size";
	private static final int PAGE_SIZE_MAX = 1000;
	private static final String ENDPOINT_TEMPLATE = "/api/v2/incremental/tickets/cursor.json";
	private final ZDClient client;
	private final int pageSize;
	private final Date startDate;
	private String cursor;

	public IncrementalExportIterator(ZDClient client, Date startDate, int pageSize) {
		this.client = Objects.requireNonNull(client);
		this.startDate = Optional.ofNullable(startDate).orElse(new Date(0));
		Validate.isTrue(pageSize == -1 || (0 < pageSize && pageSize <= PAGE_SIZE_MAX), "invalid pageSize:{}", pageSize);
		this.pageSize = pageSize == -1 ? PAGE_SIZE_MAX : pageSize;

	}

	@Override
	protected IncrementalExportResult computeNext(long index) {
		return Utils.Functions.unchecked(() -> computeNextInternal(index));
	}

	protected IncrementalExportResult computeNextInternal(long index) throws IOException {
		if (this.startDate.after(new Date()))
			return this.end();
		if (index > 0 && Utils.Strings.isBlank(cursor))
			return this.end();
		var endpointURI = getEndpointURI(index);
		var request = new RequestBuilder().setUrl(endpointURI.toString()).build();
		var result = this.client.executeRequest(request, TypeToken.of(IncrementalExportResult.class));
		if (result == null || result.isEmpty())
			return this.end();
		var nextCursor = result.getAfterCursor();
		this.cursor = nextCursor;
		return result;
	}

	protected ZDClient getClient() {
		return client;
	}

	private URI getEndpointURI(long index) {
		UrlBuilder urlb = UrlBuilder.fromUri(this.client.getEndpointURI(ENDPOINT_TEMPLATE));
		if (index == 0)
			urlb = urlb.addParameter(START_TIME_PARAM, Objects.toString(this.startDate.getTime() / 1000));
		else
			urlb = urlb.addParameter(CURSOR_PARAM, this.cursor);
		urlb = urlb.addParameter(PAGE_SIZE_PARAM, Objects.toString(this.pageSize));
		return urlb.toUri();
	}

	public static void main(String[] args) {
		var zdt = ZonedDateTime.now(ZoneOffset.UTC).truncatedTo(ChronoUnit.DAYS).minusDays(3);
		var startDate = new Date(zdt.toInstant().toEpochMilli());
		IncrementalExportIterator iter = new IncrementalExportIterator(Zendesks.get(), startDate, -1);
		StreamEx<TicketV2> ticketStream = Utils.Lots.flatMapIterables(Utils.Lots.stream(iter));
		int index = -1;
		for (TicketV2 t : ticketStream) {
			index++;
			if (index % 100 == 0)
				System.out.println(index + " - " + t.getUpdatedAt());
		}
		System.exit(0);
	}

}