package com.lfp.zendesk.client.http;

import java.io.InputStream;
import java.net.SocketAddress;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Objects;

import com.lfp.joe.utils.Utils;

import org.asynchttpclient.Response;
import org.asynchttpclient.uri.Uri;

import io.netty.handler.codec.http.HttpHeaders;
import io.netty.handler.codec.http.cookie.Cookie;

public interface KeepOpenResponse extends Response {

	public static KeepOpenResponse create(Response response) {
		Objects.requireNonNull(response);
		return Utils.Types.tryCast(response, KeepOpenResponse.class).orElseGet(() -> {
			return new KeepOpenResponse() {

				@Override
				public Response getDelegate() {
					return response;
				}

				@Override
				public String toString() {
					return response.toString();
				}
			};
		});
	}

	Response getDelegate();

	@Override
	default InputStream getResponseBodyAsStream() {
		var is = getDelegate().getResponseBodyAsStream();
		if (is == null)
			return is;
		return KeepOpenInputStream.create(is);
	}

	@Override
	default int getStatusCode() {
		return getDelegate().getStatusCode();
	}

	@Override
	default String getStatusText() {
		return getDelegate().getStatusText();
	}

	@Override
	default byte[] getResponseBodyAsBytes() {
		return getDelegate().getResponseBodyAsBytes();
	}

	@Override
	default ByteBuffer getResponseBodyAsByteBuffer() {
		return getDelegate().getResponseBodyAsByteBuffer();
	}

	@Override
	default String getResponseBody(Charset charset) {
		return getDelegate().getResponseBody(charset);
	}

	@Override
	default String getResponseBody() {
		return getDelegate().getResponseBody();
	}

	@Override
	default Uri getUri() {
		return getDelegate().getUri();
	}

	@Override
	default String getContentType() {
		return getDelegate().getContentType();
	}

	@Override
	default String getHeader(CharSequence name) {
		return getDelegate().getHeader(name);
	}

	@Override
	default List<String> getHeaders(CharSequence name) {
		return getDelegate().getHeaders(name);
	}

	@Override
	default HttpHeaders getHeaders() {
		return getDelegate().getHeaders();
	}

	@Override
	default boolean isRedirected() {
		return getDelegate().isRedirected();
	}

	@Override
	default List<Cookie> getCookies() {
		return getDelegate().getCookies();
	}

	@Override
	default boolean hasResponseStatus() {
		return getDelegate().hasResponseStatus();
	}

	@Override
	default boolean hasResponseHeaders() {
		return getDelegate().hasResponseHeaders();
	}

	@Override
	default boolean hasResponseBody() {
		return getDelegate().hasResponseBody();
	}

	@Override
	default SocketAddress getRemoteAddress() {
		return getDelegate().getRemoteAddress();
	}

	@Override
	default SocketAddress getLocalAddress() {
		return getDelegate().getLocalAddress();
	}
}
