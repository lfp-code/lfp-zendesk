package com.lfp.zendesk.client.tools;

import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;

import com.lfp.data.beans.Persistent;
import com.lfp.zendesk.core.model.TicketV2;
import com.lfp.joe.beans.joda.JodaBeans;
import com.lfp.joe.utils.Utils;

import org.joda.beans.Bean;
import org.joda.beans.BeanDefinition;
import org.joda.beans.ImmutableBean;
import org.joda.beans.JodaBeanUtils;
import org.joda.beans.MetaProperty;
import org.joda.beans.Property;
import org.joda.beans.PropertyDefinition;
import org.joda.beans.impl.direct.DirectFieldsBeanBuilder;
import org.joda.beans.impl.direct.DirectMetaBean;
import org.joda.beans.impl.direct.DirectMetaProperty;
import org.joda.beans.impl.direct.DirectMetaPropertyMap;
import org.zendesk.client.v2.model.Ticket;

import com.google.common.collect.ImmutableList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import at.favre.lib.bytes.Bytes;
import one.util.streamex.StreamEx;

@BeanDefinition
public final class IncrementalExportResult extends Persistent.Impl implements ImmutableBean, Iterable<TicketV2> {

    @SerializedName("tickets")
    @Expose
    @PropertyDefinition
    private final List<TicketV2> tickets;
    @SerializedName("after_cursor")
    @Expose
    @PropertyDefinition(get = "manual")
    private final String afterCursor;
    @SerializedName("before_cursor")
    @Expose
    @PropertyDefinition(get = "manual")
    private final String beforeCursor;

    @Override
    public Iterator<TicketV2> iterator() {
        return streamTickets(this.tickets).iterator();
    }

    public boolean contains(Ticket ticket) {
        if (ticket == null)
            return false;
        Function<Ticket, Bytes> hasher = t -> {
            var idBytes = Utils.Bits.serialize(ticket.getId());
            Date updatedAt;
            if (t instanceof TicketV2)
                updatedAt = ((TicketV2) t).getUpdatedAt(true);
            else
                updatedAt = t.getUpdatedAt();
            var updatedAtBytes = updatedAt == null ? null : Utils.Bits.serialize(updatedAt);
            return Utils.Crypto.hashMD5(idBytes, updatedAtBytes);
        };
        var ticketHash = hasher.apply(ticket);
        for (var checkTicket : this) {
            if (hasher.apply(checkTicket).equals(ticketHash))
                return true;
        }
        return false;
    }

    // -----------------------------------------------------------------------
    /**
     * Gets the afterCursor.
     * 
     * @return the value of the property
     */
    public String getAfterCursor() {
        return Utils.Strings.isBlank(afterCursor) ? null : afterCursor;
    }

    // -----------------------------------------------------------------------
    /**
     * Gets the beforeCursor.
     * 
     * @return the value of the property
     */
    public String getBeforeCursor() {
        return Utils.Strings.isBlank(beforeCursor) ? null : beforeCursor;
    }

    public int size() {
        return Optional.ofNullable(this.tickets).map(v -> v.size()).orElse(0);
    }

    public boolean isEmpty() {
        return this.size() == 0;
    }

    public Optional<Date> getLatestUpdatedAt() {
        var stream = Utils.Lots.stream(tickets).nonNull().map(v -> v.getUpdatedAt(true));
        stream = stream.nonNull();
        return stream.sortedBy(v -> v.getTime() * -1).findFirst();
    }

    public IncrementalExportResult filterToDay(Date date) {
        if (date == null)
            return filter(null);
        var dayStart = Utils.Times.startOfDay(Utils.Times.toDateTime(date));
        var dayEnd = dayStart.plus(1, ChronoUnit.DAYS);
        return filterToDate(Date.from(dayStart.toInstant()), Date.from(dayEnd.toInstant()));
    }

    public IncrementalExportResult filterToDate(Date startDate, Date endDate) {
        if (startDate == null && endDate == null)
            return filter(null);
        return filter(v -> {
            var updatedAt = v.getUpdatedAt(true);
            if (updatedAt == null)
                return false;
            if (startDate != null && updatedAt.getTime() < startDate.getTime())
                return false;
            if (endDate != null && updatedAt.getTime() >= endDate.getTime())
                return false;
            return true;
        });
    }

    public IncrementalExportResult filter(Predicate<TicketV2> filter) {
        if (filter == null)
            return this;
        var ticketStream = Utils.Lots.stream(this.tickets).filter(filter);
        ticketStream = streamTickets(ticketStream);
        var tickets = ticketStream.toList();
        if (tickets.size() == this.size())
            return this;
        var blder = this.toBuilder();
        blder.tickets(tickets);
        blder.beforeCursor(null);
        blder.afterCursor(null);
        var result = blder.build();
        result.updateCreatedAt(this.getCreatedAt());
        return result;
    }

    public IncrementalExportResult update(IncrementalExportResult other) {
        if (other == null)
            return this;
        var ticketStream = Utils.Lots.stream(this.tickets).append(Utils.Lots.stream(other));
        ticketStream = streamTickets(ticketStream);
        var tickets = ticketStream.toList();
        var blder = this.toBuilder();
        blder.tickets(tickets);
        blder.beforeCursor(null);
        blder.afterCursor(null);
        var result = blder.build();
        return result;
    }

    private static StreamEx<TicketV2> streamTickets(Iterable<TicketV2> tickets) {
        var stream = Utils.Lots.stream(tickets);
        stream = stream.nonNull();
        stream = stream.filter(v -> v.getId() != null);
        stream = stream.sorted(TicketV2.UPDATED_AT_COMPARATOR);
        stream = stream.distinct(v -> v.getId());
        return stream;
    }

    public static void main(String[] args) {
        JodaBeans.updateCode();
    }

    //------------------------- AUTOGENERATED START -------------------------
    ///CLOVER:OFF
    /**
     * The meta-bean for {@code IncrementalExportResult}.
     * @return the meta-bean, not null
     */
    public static IncrementalExportResult.Meta meta() {
        return IncrementalExportResult.Meta.INSTANCE;
    }

    static {
        JodaBeanUtils.registerMetaBean(IncrementalExportResult.Meta.INSTANCE);
    }

    /**
     * Returns a builder used to create an instance of the bean.
     * @return the builder, not null
     */
    public static IncrementalExportResult.Builder builder() {
        return new IncrementalExportResult.Builder();
    }

    private IncrementalExportResult(
            List<TicketV2> tickets,
            String afterCursor,
            String beforeCursor) {
        this.tickets = (tickets != null ? ImmutableList.copyOf(tickets) : null);
        this.afterCursor = afterCursor;
        this.beforeCursor = beforeCursor;
    }

    @Override
    public IncrementalExportResult.Meta metaBean() {
        return IncrementalExportResult.Meta.INSTANCE;
    }

    @Override
    public <R> Property<R> property(String propertyName) {
        return metaBean().<R>metaProperty(propertyName).createProperty(this);
    }

    @Override
    public Set<String> propertyNames() {
        return metaBean().metaPropertyMap().keySet();
    }

    //-----------------------------------------------------------------------
    /**
     * Gets the tickets.
     * @return the value of the property
     */
    public List<TicketV2> getTickets() {
        return tickets;
    }

    //-----------------------------------------------------------------------
    /**
     * Returns a builder that allows this bean to be mutated.
     * @return the mutable builder, not null
     */
    public Builder toBuilder() {
        return new Builder(this);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj != null && obj.getClass() == this.getClass()) {
            IncrementalExportResult other = (IncrementalExportResult) obj;
            return JodaBeanUtils.equal(tickets, other.tickets) &&
                    JodaBeanUtils.equal(afterCursor, other.afterCursor) &&
                    JodaBeanUtils.equal(beforeCursor, other.beforeCursor);
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = getClass().hashCode();
        hash = hash * 31 + JodaBeanUtils.hashCode(tickets);
        hash = hash * 31 + JodaBeanUtils.hashCode(afterCursor);
        hash = hash * 31 + JodaBeanUtils.hashCode(beforeCursor);
        return hash;
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder(128);
        buf.append("IncrementalExportResult{");
        buf.append("tickets").append('=').append(tickets).append(',').append(' ');
        buf.append("afterCursor").append('=').append(afterCursor).append(',').append(' ');
        buf.append("beforeCursor").append('=').append(JodaBeanUtils.toString(beforeCursor));
        buf.append('}');
        return buf.toString();
    }

    //-----------------------------------------------------------------------
    /**
     * The meta-bean for {@code IncrementalExportResult}.
     */
    public static final class Meta extends DirectMetaBean {
        /**
         * The singleton instance of the meta-bean.
         */
        static final Meta INSTANCE = new Meta();

        /**
         * The meta-property for the {@code tickets} property.
         */
        @SuppressWarnings({"unchecked", "rawtypes" })
        private final MetaProperty<List<TicketV2>> tickets = DirectMetaProperty.ofImmutable(
                this, "tickets", IncrementalExportResult.class, (Class) List.class);
        /**
         * The meta-property for the {@code afterCursor} property.
         */
        private final MetaProperty<String> afterCursor = DirectMetaProperty.ofImmutable(
                this, "afterCursor", IncrementalExportResult.class, String.class);
        /**
         * The meta-property for the {@code beforeCursor} property.
         */
        private final MetaProperty<String> beforeCursor = DirectMetaProperty.ofImmutable(
                this, "beforeCursor", IncrementalExportResult.class, String.class);
        /**
         * The meta-properties.
         */
        private final Map<String, MetaProperty<?>> metaPropertyMap$ = new DirectMetaPropertyMap(
                this, null,
                "tickets",
                "afterCursor",
                "beforeCursor");

        /**
         * Restricted constructor.
         */
        private Meta() {
        }

        @Override
        protected MetaProperty<?> metaPropertyGet(String propertyName) {
            switch (propertyName.hashCode()) {
                case -1322977561:  // tickets
                    return tickets;
                case -230346670:  // afterCursor
                    return afterCursor;
                case -381590603:  // beforeCursor
                    return beforeCursor;
            }
            return super.metaPropertyGet(propertyName);
        }

        @Override
        public IncrementalExportResult.Builder builder() {
            return new IncrementalExportResult.Builder();
        }

        @Override
        public Class<? extends IncrementalExportResult> beanType() {
            return IncrementalExportResult.class;
        }

        @Override
        public Map<String, MetaProperty<?>> metaPropertyMap() {
            return metaPropertyMap$;
        }

        //-----------------------------------------------------------------------
        /**
         * The meta-property for the {@code tickets} property.
         * @return the meta-property, not null
         */
        public MetaProperty<List<TicketV2>> tickets() {
            return tickets;
        }

        /**
         * The meta-property for the {@code afterCursor} property.
         * @return the meta-property, not null
         */
        public MetaProperty<String> afterCursor() {
            return afterCursor;
        }

        /**
         * The meta-property for the {@code beforeCursor} property.
         * @return the meta-property, not null
         */
        public MetaProperty<String> beforeCursor() {
            return beforeCursor;
        }

        //-----------------------------------------------------------------------
        @Override
        protected Object propertyGet(Bean bean, String propertyName, boolean quiet) {
            switch (propertyName.hashCode()) {
                case -1322977561:  // tickets
                    return ((IncrementalExportResult) bean).getTickets();
                case -230346670:  // afterCursor
                    return ((IncrementalExportResult) bean).getAfterCursor();
                case -381590603:  // beforeCursor
                    return ((IncrementalExportResult) bean).getBeforeCursor();
            }
            return super.propertyGet(bean, propertyName, quiet);
        }

        @Override
        protected void propertySet(Bean bean, String propertyName, Object newValue, boolean quiet) {
            metaProperty(propertyName);
            if (quiet) {
                return;
            }
            throw new UnsupportedOperationException("Property cannot be written: " + propertyName);
        }

    }

    //-----------------------------------------------------------------------
    /**
     * The bean-builder for {@code IncrementalExportResult}.
     */
    public static final class Builder extends DirectFieldsBeanBuilder<IncrementalExportResult> {

        private List<TicketV2> tickets;
        private String afterCursor;
        private String beforeCursor;

        /**
         * Restricted constructor.
         */
        private Builder() {
        }

        /**
         * Restricted copy constructor.
         * @param beanToCopy  the bean to copy from, not null
         */
        private Builder(IncrementalExportResult beanToCopy) {
            this.tickets = (beanToCopy.getTickets() != null ? ImmutableList.copyOf(beanToCopy.getTickets()) : null);
            this.afterCursor = beanToCopy.getAfterCursor();
            this.beforeCursor = beanToCopy.getBeforeCursor();
        }

        //-----------------------------------------------------------------------
        @Override
        public Object get(String propertyName) {
            switch (propertyName.hashCode()) {
                case -1322977561:  // tickets
                    return tickets;
                case -230346670:  // afterCursor
                    return afterCursor;
                case -381590603:  // beforeCursor
                    return beforeCursor;
                default:
                    throw new NoSuchElementException("Unknown property: " + propertyName);
            }
        }

        @SuppressWarnings("unchecked")
        @Override
        public Builder set(String propertyName, Object newValue) {
            switch (propertyName.hashCode()) {
                case -1322977561:  // tickets
                    this.tickets = (List<TicketV2>) newValue;
                    break;
                case -230346670:  // afterCursor
                    this.afterCursor = (String) newValue;
                    break;
                case -381590603:  // beforeCursor
                    this.beforeCursor = (String) newValue;
                    break;
                default:
                    throw new NoSuchElementException("Unknown property: " + propertyName);
            }
            return this;
        }

        @Override
        public Builder set(MetaProperty<?> property, Object value) {
            super.set(property, value);
            return this;
        }

        /**
         * @deprecated Use Joda-Convert in application code
         */
        @Override
        @Deprecated
        public Builder setString(String propertyName, String value) {
            setString(meta().metaProperty(propertyName), value);
            return this;
        }

        /**
         * @deprecated Use Joda-Convert in application code
         */
        @Override
        @Deprecated
        public Builder setString(MetaProperty<?> property, String value) {
            super.setString(property, value);
            return this;
        }

        /**
         * @deprecated Loop in application code
         */
        @Override
        @Deprecated
        public Builder setAll(Map<String, ? extends Object> propertyValueMap) {
            super.setAll(propertyValueMap);
            return this;
        }

        @Override
        public IncrementalExportResult build() {
            return new IncrementalExportResult(
                    tickets,
                    afterCursor,
                    beforeCursor);
        }

        //-----------------------------------------------------------------------
        /**
         * Sets the tickets.
         * @param tickets  the new value
         * @return this, for chaining, not null
         */
        public Builder tickets(List<TicketV2> tickets) {
            this.tickets = tickets;
            return this;
        }

        /**
         * Sets the {@code tickets} property in the builder
         * from an array of objects.
         * @param tickets  the new value
         * @return this, for chaining, not null
         */
        public Builder tickets(TicketV2... tickets) {
            return tickets(ImmutableList.copyOf(tickets));
        }

        /**
         * Sets the afterCursor.
         * @param afterCursor  the new value
         * @return this, for chaining, not null
         */
        public Builder afterCursor(String afterCursor) {
            this.afterCursor = afterCursor;
            return this;
        }

        /**
         * Sets the beforeCursor.
         * @param beforeCursor  the new value
         * @return this, for chaining, not null
         */
        public Builder beforeCursor(String beforeCursor) {
            this.beforeCursor = beforeCursor;
            return this;
        }

        //-----------------------------------------------------------------------
        @Override
        public String toString() {
            StringBuilder buf = new StringBuilder(128);
            buf.append("IncrementalExportResult.Builder{");
            buf.append("tickets").append('=').append(JodaBeanUtils.toString(tickets)).append(',').append(' ');
            buf.append("afterCursor").append('=').append(JodaBeanUtils.toString(afterCursor)).append(',').append(' ');
            buf.append("beforeCursor").append('=').append(JodaBeanUtils.toString(beforeCursor));
            buf.append('}');
            return buf.toString();
        }

    }

    ///CLOVER:ON
    //-------------------------- AUTOGENERATED END --------------------------
}
