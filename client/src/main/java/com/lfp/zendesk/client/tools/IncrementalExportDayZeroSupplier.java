package com.lfp.zendesk.client.tools;

import java.io.IOException;
import java.time.Duration;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Supplier;

import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.client.RedissonClientLFP;
import com.lfp.data.redisson.client.codec.GsonCodec;
import com.lfp.data.redisson.tools.accessor.SemaphoreAccessor;
import com.lfp.data.tendis.client.TendisClients;
import com.lfp.zendesk.client.ZDClient;
import com.lfp.zendesk.client.Zendesks;
import com.lfp.zendesk.core.model.TicketV2;
import com.lfp.joe.cache.StatValue;
import com.lfp.joe.utils.Utils;

import org.redisson.api.RBucket;

import one.util.streamex.StreamEx;

public class IncrementalExportDayZeroSupplier implements Supplier<Optional<Date>> {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 1;
	public static final Duration CACHE_TTL = Duration.ofDays(7);
	public static final Duration CACHE_TTL_EMPTY = Duration.ofMinutes(1);
	private final ZDClient client;
	private final RedissonClientLFP storageClient;
	private final GsonCodec gsonCodec;

	public IncrementalExportDayZeroSupplier(ZDClient client) {
		this(client, TendisClients.getDefault());
	}

	public IncrementalExportDayZeroSupplier(ZDClient client, RedissonClientLFP storageClient) {
		this.client = Objects.requireNonNull(client);
		this.storageClient = Objects.requireNonNull(storageClient);
		this.gsonCodec = new GsonCodec(client.getGson());
	}

	@Override
	public Optional<Date> get() {
		var result = Utils.Functions.unchecked(() -> getInternal(false));
		return Optional.ofNullable(result);
	}

	protected Date getInternal(boolean locked) throws IOException {
		RBucket<StatValue<Date>> bucket = storageClient.getBucket(generateKey("bucket"), this.gsonCodec);
		var result = bucket.get();
		if (result != null)
			return result.getValue();
		if (!locked) {
			var accessor = SemaphoreAccessor.create(1, Duration.ofSeconds(5), generateKey("lock"));
			try {
				return accessor.access(() -> getInternal(true));
			} catch (InterruptedException e) {
				throw Utils.Exceptions.as(e, IOException.class);
			}
		}
		var iter = new IncrementalExportIterator(this.client, null, 10);
		StreamEx<TicketV2> ticketStream = !iter.hasNext() ? StreamEx.empty() : Utils.Lots.stream(iter.next());
		var dateStreams = ticketStream.map(v -> {
			return Utils.Lots.stream(v.getCreatedAt(), v.getUpdatedAt(false), v.getUpdatedAt(true));
		});
		var earliestDateOp = Utils.Lots.flatMap(dateStreams).nonNull().sortedBy(v -> v.getTime()).findFirst();
		earliestDateOp = earliestDateOp.map(v -> new Date(v.getTime() - Duration.ofDays(1).toMillis()));
		var earliestDate = earliestDateOp.orElse(null);
		var expiration = earliestDate != null ? CACHE_TTL : CACHE_TTL_EMPTY;
		bucket.set(StatValue.build(earliestDate), expiration.toMillis(), TimeUnit.MILLISECONDS);
		return earliestDate;
	}

	private String generateKey(Object... append) {
		StreamEx<Object> stream = Utils.Lots.stream(THIS_CLASS, VERSION, this.client.getURI());
		stream = stream.append(Utils.Lots.stream(append));
		return RedisUtils.generateKey(stream.toArray());
	}

	public static void main(String[] args) {
		System.out.println(new IncrementalExportDayZeroSupplier(Zendesks.get()).get());
		System.exit(0);
	}
}
