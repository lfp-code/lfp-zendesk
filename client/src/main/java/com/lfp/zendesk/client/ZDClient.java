package com.lfp.zendesk.client;

import java.io.IOException;
import java.net.URI;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;

import com.lfp.data.redisson.tools.net.RedissonCookieStore;
import com.lfp.data.tendis.client.TendisClients;
import com.lfp.joe.core.function.MemoizedSupplier;
import com.lfp.joe.core.properties.Configs;
import com.lfp.joe.net.http.headers.Headers;
import com.lfp.joe.net.http.ip.IPs;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.net.status.StatusCodes;
import com.lfp.joe.okhttp.Ok;
import com.lfp.joe.okhttp.cookie.CookieJars;
import com.lfp.joe.okhttp.interceptor.BrowserHeaderInterceptor;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.serial.gson.EnumTypeAdapterFactory;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.core.function.Throws.ThrowingSupplier;
import com.lfp.mail.core.EmailMessage;
import com.lfp.zendesk.client.cache.AuditEmailCache;
import com.lfp.zendesk.client.config.ZendeskConfig;
import com.lfp.zendesk.client.http.KeepOpenResponse;
import com.lfp.zendesk.client.session.ZD404Interceptor;
import com.lfp.zendesk.client.session.ZDSessionInterceptor;
import com.lfp.zendesk.core.ZDUtils;
import com.lfp.zendesk.core.model.EventContext;
import com.lfp.zendesk.core.model.RequestTypeAdapterFactory;
import com.lfp.zendesk.core.model.TicketV2;

import org.apache.commons.lang3.StringUtils;
import org.asynchttpclient.AsyncCompletionHandler;
import org.asynchttpclient.AsyncHandler;
import org.asynchttpclient.AsyncHttpClient;
import org.asynchttpclient.ListenableFuture;
import org.asynchttpclient.Realm;
import org.asynchttpclient.Request;
import org.asynchttpclient.RequestBuilder;
import org.asynchttpclient.Response;
import org.zendesk.client.v2.Zendesk;
import org.zendesk.client.v2.model.Audit;
import org.zendesk.client.v2.model.events.Event;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.reflect.TypeToken;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;

import io.mikael.urlbuilder.UrlBuilder;
import okhttp3.OkHttpClient;
import one.util.streamex.EntryStream;
import one.util.streamex.StreamEx;

@SuppressWarnings({ "unchecked", "deprecation" })
public interface ZDClient {

	Zendesk getZendesk();

	Gson getGson();

	OkHttpClient getWebClient();

	default AsyncHttpClient getClient() {
		var result = ZDReflections.Zendesk_client_FA.get(this.getZendesk());
		return result;
	}

	default ObjectMapper getMapper() {
		var result = ZDReflections.Zendesk_mapper_FA.get(this.getZendesk());
		return result;
	}

	default Optional<Realm> getRealm() {
		var result = ZDReflections.Zendesk_realm_FA.get(this.getZendesk());
		return Optional.ofNullable(result);
	}

	default Optional<String> getOauthToken() {
		var result = ZDReflections.Zendesk_oauthToken_FA.get(this.getZendesk());
		return Utils.Strings.trimToNullOptional(result);
	}

	default URI getURI() {
		var url = ZDReflections.Zendesk_url_FA.get(this.getZendesk());
		if (Utils.Strings.isBlank(url))
			return null;
		return URIs.normalize(URI.create(url));
	}

	default URI getApiURI() {
		return UrlBuilder.fromUri(getURI()).withPath("/api/v2").toUri();
	}

	default String getAuthorizationHeaderValue() {
		Optional<Realm> realmOp = this.getRealm();
		if (!realmOp.isEmpty())
			return Headers.basicAuthorization(realmOp.get().getPrincipal(), realmOp.get().getPassword());
		return "Bearer " + getOauthToken().get();
	}

	default URI getEndpointURI(String endpoint) {
		Supplier<IllegalArgumentException> errorGetter = () -> {
			return new IllegalArgumentException("unable to parse endpoint uri:" + endpoint);
		};
		String toParse = Utils.Strings.trimToNull(endpoint);
		if (toParse == null)
			throw errorGetter.get();
		final URI apiURI = getApiURI();
		{// check if already uri
			var endpointURI = URIs.parse(toParse).orElse(null);
			endpointURI = URIs.normalize(endpointURI);
			if (endpointURI != null && Utils.Strings.equals(apiURI.getHost(), endpointURI.getHost()))
				toParse = Utils.Strings.substringAfter(endpointURI.toString(), "://" + endpointURI.getHost());
		}
		toParse = Utils.Strings.trimToNull(toParse);
		if (toParse == null)
			throw errorGetter.get();
		{// check if already uri
			var endpointURI = URIs.parse(toParse).orElse(null);
			endpointURI = URIs.normalize(endpointURI);
			if (endpointURI != null && Utils.Strings.equals(apiURI.getHost(), endpointURI.getHost()))
				toParse = Utils.Strings.substringAfter(endpointURI.toString(), "://" + endpointURI.getHost());
		}
		final String apiPath = apiURI.getPath();
		{// remove api path from endpoint
			toParse = URIs.normalizePath(toParse);
			if (URIs.pathPrefixMatch(toParse, apiPath)) {
				toParse = StringUtils.substringAfter(toParse, apiPath);
				toParse = URIs.normalizePath(toParse);
			}
		}
		if (Utils.Strings.isBlank(toParse) || "/".equals(toParse))
			throw errorGetter.get();
		URI endpointURI = URIs.parse(apiURI.toString() + toParse).get();
		return endpointURI;
	}

	@SuppressWarnings("serial")
	default StreamEx<TicketV2> streamTickets(Iterable<Long> ticketIds) {
		var tidStream = Utils.Lots.stream(ticketIds).nonNull().filter(v -> v >= 0);
		var tidBatchStream = Utils.Lots.buffer(tidStream, b -> b.size() >= 100);
		var typeToken = new TypeToken<List<TicketV2>>() {
		};
		var ticketBatchStream = tidBatchStream.map(batch -> {
			try {
				if (batch.size() == 1) {
					var uri = getEndpointURI(String.format("/tickets/%s.json", batch.get(0)));
					var rb = new RequestBuilder().setUrl(uri.toString());
					TicketV2 ticket;
					ticket = this.executeRequest(rb.build(), TypeToken.of(TicketV2.class));
					return Utils.Lots.stream(Arrays.asList(ticket));
				}
				var urlb = UrlBuilder.fromUri(getEndpointURI("/tickets/show_many.json"));
				urlb = urlb.addParameter("ids", Utils.Lots.stream(batch).joining(","));
				var rb = new RequestBuilder().setUrl(urlb.toString());
				return Utils.Lots.stream(this.executeRequest(rb.build(), typeToken));
			} catch (IOException e) {
				throw Utils.Exceptions.asRuntimeException(e);
			}
		});
		StreamEx<TicketV2> ticketStream = Utils.Lots.flatMap(ticketBatchStream).nonNull();
		return ticketStream;
	}

	default StreamEx<EventContext> streamTicketEventContexts(long ticketId) {
		StreamEx<Audit> auditStream = Utils.Lots.stream(this.getZendesk().getTicketAudits(ticketId)).nonNull();
		StreamEx<EntryStream<Audit, Event>> estreams = auditStream.map(v -> {
			StreamEx<Event> eventStream = Utils.Lots.stream(v.getEvents());
			EntryStream<Audit, Event> estream = eventStream.mapToEntry(vv -> v).invert();
			return estream;
		});
		var estream = Utils.Lots.flatMap(estreams);
		Map<Long, Optional<EmailMessage>> emailMesageCache = new ConcurrentHashMap<>();
		return estream.map(ent -> {
			ThrowingSupplier<EmailMessage, IOException> emailMessageGetter = () -> {
				var emailMessageOp = Utils.Functions.computeIfAbsent(emailMesageCache, ent.getKey().getId(),
						auditId -> {
							return getEmailMessage(ticketId, auditId);
						});
				return emailMessageOp.orElse(null);
			};
			return new EventContext(ticketId, ent.getKey(), ent.getValue(), emailMessageGetter);
		});
	}

	default JsonElement executeRequest(Request request, int... validStatusCodes) throws IOException {
		return executeRequest(request, TypeToken.of(JsonElement.class), validStatusCodes);
	}

	default <T> T executeRequest(Request request, TypeToken<T> responseTypeToken, int... validStatusCodes)
			throws IOException {
		AsyncHandler<T> asyncHandler = new AsyncCompletionHandler<T>() {

			private int attemptIndex = 0;

			@Override
			public T onCompleted(Response response) throws Exception {
				var responseStatusCode = response.getStatusCode();
				var cfg = Configs.get(ZendeskConfig.class);
				if (responseStatusCode / 100 == 5 && attemptIndex < cfg.serverErrorRetryCount()) {
					attemptIndex++;
					Threads.sleepUnchecked(cfg.serverErrorRetryDelay());
					return executeRequest(request, this);
				}
				response = KeepOpenResponse.create(response);
				if (!StatusCodes.isSuccess(responseStatusCode,
						Utils.Lots.stream(validStatusCodes).prepend(2, 404, 3).distinct().toArray()))
					throw new IOException(ZDUtils.getSummary(request, response));
				var gson = getGson();
				if (!responseTypeToken.isSubtypeOf(Collection.class))
					return Serials.Gsons.fromStream(gson, response.getResponseBodyAsStream(), responseTypeToken);
				var je = Serials.Gsons.fromStream(gson, response.getResponseBodyAsStream(),
						TypeToken.of(JsonElement.class));
				Supplier<T> fallback = () -> gson.fromJson(je, responseTypeToken.getType());
				if (je == null || !je.isJsonObject())
					return fallback.get();
				var resultOp = Utils.Functions.catching(() -> Optional.ofNullable(fallback.get()), t -> null);
				if (resultOp != null)
					return resultOp.orElse(null);
				var path = Optional.ofNullable(response.getUri()).map(v -> v.getPath()).orElse(null);
				if (path == null)
					return fallback.get();
				var jo = je.getAsJsonObject();
				for (var key : jo.keySet()) {
					if (Utils.Strings.containsIgnoreCase(path, String.format("/%s/", key))
							|| Utils.Strings.containsIgnoreCase(path, String.format("/%ss/", key)))
						return gson.fromJson(jo.get(key), responseTypeToken.getType());
				}
				return fallback.get();
			}
		};
		return executeRequest(request, asyncHandler);
	}

	default <T> T executeRequest(Request request, AsyncHandler<T> asyncHandler) throws IOException {
		Objects.requireNonNull(request, "request required");
		RequestBuilder rb = request.toBuilder();
		rb.addHeader(Headers.AUTHORIZATION, getAuthorizationHeaderValue());
		request = rb.build();
		ListenableFuture<T> future = this.getClient().executeRequest(request, asyncHandler);
		try {
			return future.get();
		} catch (Throwable t) {
			t = Utils.Exceptions.unwrapExecutionExceptions(t).orElse(t);
			throw Utils.Exceptions.as(t, IOException.class);
		}
	}

	default Optional<EmailMessage> getEmailMessage(long ticketId, long auditId) throws IOException {
		return getEmailMessage(ticketId, auditId, false);
	}

	Optional<EmailMessage> getEmailMessage(long ticketId, long auditId, boolean refreshCache) throws IOException;

	public static class Impl implements ZDClient {
		private static final Class<?> THIS_CLASS = new Object() {
		}.getClass().getEnclosingClass();
		private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
		private static final int VERSION = 1;
		private final AuditEmailCache auditEmailCache = new AuditEmailCache(this);
		private final MemoizedSupplier<Zendesk> zendeskSupplier;
		private final MemoizedSupplier<Gson> gsonSupplier;
		private final MemoizedSupplier<OkHttpClient> webClientSupplier;

		public Impl(Callable<Zendesk> zendeskCallable) {
			this.zendeskSupplier = Utils.Functions.memoize(Objects.requireNonNull(zendeskCallable), null);
			this.gsonSupplier = Utils.Functions.memoize(() -> createGsonBuilder().create());
			this.webClientSupplier = Utils.Functions.memoize(() -> createWebClient());
		}

		@Override
		public Zendesk getZendesk() {
			return this.zendeskSupplier.get();
		}

		@Override
		public Gson getGson() {
			return gsonSupplier.get();
		}

		@Override
		public OkHttpClient getWebClient() {
			return webClientSupplier.get();
		}

		@Override
		public Optional<EmailMessage> getEmailMessage(long ticketId, long auditId, boolean refreshCache)
				throws IOException {
			return auditEmailCache.get(ticketId, auditId, refreshCache);
		}

		protected GsonBuilder createGsonBuilder() {
			var gsb = Serials.Gsons.getBuilder();
			gsb.registerTypeAdapterFactory(new RequestTypeAdapterFactory() {

				@Override
				public ObjectMapper getObjectMapper() {
					return getMapper();
				}
			});
			gsb.registerTypeAdapterFactory(new EnumTypeAdapterFactory(
					EnumTypeAdapterFactory.Options.builder().withForceLowerCase(true).build()));
			gsb.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
			return gsb;
		}

		protected OkHttpClient createWebClient() {
			var cookieStore = new RedissonCookieStore(TendisClients.getDefault(), THIS_CLASS, VERSION,
					IPs.getIPAddress(), "cookie-store");
			var okb = Ok.Clients.get().newBuilder();
			okb.cookieJar(CookieJars.create(cookieStore));
			okb.addNetworkInterceptor(new ZD404Interceptor(this));
			okb.addInterceptor(BrowserHeaderInterceptor.getBrowserDefault());
			okb.addInterceptor(new ZDSessionInterceptor(this));
			var client = okb.build();
			return client;
		}

	}

}
