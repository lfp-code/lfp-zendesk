package com.lfp.zendesk.client.tools;

import java.time.Duration;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

import com.lfp.data.redis.RedisUtils;
import com.lfp.data.redisson.client.RedissonClients;
import com.lfp.zendesk.client.ZDClient;
import com.lfp.zendesk.client.Zendesks;
import com.lfp.joe.serial.Serials;
import com.lfp.joe.threads.Threads;
import com.lfp.joe.utils.Utils;
import com.lfp.joe.utils.time.TimeParser;

import org.redisson.RedissonLock;

import com.github.throwable.beanref.BeanRef;

import one.util.streamex.StreamEx;

public class IncrementalExportDayFunction implements Function<Date, StreamEx<IncrementalExportResult>> {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private final ZDClient client;

	public IncrementalExportDayFunction(ZDClient client) {
		this.client = Objects.requireNonNull(client);
	}

	@Override
	public StreamEx<IncrementalExportResult> apply(Date startDate) {
		return apply(startDate, false);
	}

	protected StreamEx<IncrementalExportResult> apply(Date startDate, boolean disableStartDateTruncate) {
		var ible = applyInternal(startDate, disableStartDateTruncate);
		var stream = Utils.Lots.stream(ible);
		stream = stream.nonNull();
		stream = stream.ifEmpty(IncrementalExportResult.builder().build());
		return stream;
	}

	protected Iterable<IncrementalExportResult> applyInternal(Date startDate, boolean disableStartDateTruncate) {
		startDate = Optional.ofNullable(startDate).orElse(new Date(0));
		if (startDate.getTime() < 0 || startDate.after(new Date()))
			return null;
		var startTime = disableStartDateTruncate ? Utils.Times.toDateTime(startDate)
				: Utils.Times.startOfDay(Utils.Times.toDateTime(startDate));
		var endTime = Utils.Times.startOfDay(Utils.Times.toDateTime(startDate)).plus(1, ChronoUnit.DAYS);
		var iter = new IncrementalExportIterator(this.client, Utils.Times.toDate(startTime), -1);
		return Utils.Lots.stream(il -> {
			if (!iter.hasNext())
				return il.end();
			var record = iter.next();
			var latestUpdatedAt = record.getLatestUpdatedAt().orElse(null);
			if (latestUpdatedAt == null || latestUpdatedAt.after(Utils.Times.toDate(endTime)))
				return il.end(record);
			return record;
		});
	}

	protected ZDClient getClient() {
		return client;
	}

	public static void main(String[] args) {
		var client = RedissonClients.getDefault();
		var rwLock = client.getReadWriteLock(RedisUtils.generateKey(THIS_CLASS, "lock-test-1"));
		RedissonLock rlock = (RedissonLock) rwLock.readLock();
		rlock.lock(Duration.ofSeconds(10).toMillis(), TimeUnit.MILLISECONDS);
		System.out.println(rlock.remainTimeToLive());
		Threads.sleepUnchecked(Duration.ofSeconds(5));
		System.out.println(rlock.remainTimeToLive());
		rlock.expire(Duration.ofSeconds(10).toMillis(), TimeUnit.MILLISECONDS);
		System.out.println(rlock.remainTimeToLive());
		var rl2 = rwLock.readLock();
		rl2.lock();
		try {
			System.out.println("READ LOCKED");
		} finally {
			rl2.unlock();
		}
		var wl = rwLock.writeLock();
		wl.lock();
		try {
			System.out.println("WRITE LOCKED");
		} finally {
			wl.unlock();
		}
		Function<IncrementalExportResult, String> summaryGetter = r -> {
			var printRecord = r.toBuilder().tickets(List.of()).build();
			var jo = Zendesks.get().getGson().toJsonTree(printRecord).getAsJsonObject();
			var meta = IncrementalExportResult.meta();
			jo.remove(meta.tickets().name());
			jo.addProperty(meta.tickets().name(), String.format("[%s]", r.size()));
			var latestUpdatedAt = r.getLatestUpdatedAt().orElse(null);
			if (latestUpdatedAt != null)
				jo.addProperty(BeanRef.$(IncrementalExportResult::getLatestUpdatedAt).getPath(),
						latestUpdatedAt.toString());
			return Serials.Gsons.getPretty().toJson(jo);
		};
		var startDate = TimeParser.instance().tryParseDate("2020-12-21").get();
		var reader = new IncrementalExportDayFunction(Zendesks.get());
		var stream = reader.apply(startDate);
		for (var record : stream)
			System.out.println(summaryGetter.apply(record));
		System.exit(0);
	}

}