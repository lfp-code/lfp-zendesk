package com.lfp.zendesk.client.http;

import java.net.URI;
import java.time.Duration;
import java.util.Objects;
import java.util.concurrent.locks.ReentrantLock;

import com.lfp.data.redisson.tools.accessor.RateLimiterAccessor;
import com.lfp.joe.net.http.uri.URIs;
import com.lfp.joe.utils.Utils;

import org.apache.commons.lang3.Validate;
import org.asynchttpclient.filter.FilterContext;
import org.asynchttpclient.filter.FilterException;
import org.asynchttpclient.filter.RequestFilter;

public class ZendeskRateLimiter implements RequestFilter {
	private static final Class<?> THIS_CLASS = new Object() {
	}.getClass().getEnclosingClass();
	private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory.getLogger(THIS_CLASS);
	private static final int VERSION = 0;
	// we don't want calls to skip in line, can lead to blocking
	private final ReentrantLock localLock = new ReentrantLock(true);
	private final boolean highVolumeAddOnEnabled;

	public ZendeskRateLimiter(boolean highVolumeAddOnEnabled) {
		this.highVolumeAddOnEnabled = highVolumeAddOnEnabled;
	}

	@Override
	public <T> FilterContext<T> filter(FilterContext<T> ctx) throws FilterException {
		var requestURI = URIs.parse(ctx.getRequest().getUrl()).orElse(null);
		var rateLimitValue = getRateLimitValue(ctx.getRequest().getMethod(), requestURI);
		if (rateLimitValue == null)
			return ctx;
		localLock.lock();
		try {
			var accessor = RateLimiterAccessor.create(rateLimitValue.permits, rateLimitValue.leaseDuration, THIS_CLASS,
					"rate-limit", VERSION, rateLimitValue.id);
			return accessor.access(() -> ctx);
		} catch (InterruptedException | RuntimeException e) {
			throw Utils.Exceptions.as(e, FilterException.class);
		}finally {
			localLock.unlock();
		}
	}

	// https://developer.zendesk.com/rest_api/docs/support/usage_limits#endpoint-rate-limits
	protected RateLimitValue getRateLimitValue(String method, URI requestURI) {
		var path = URIs.normalizePath(requestURI);
		if (!highVolumeAddOnEnabled && URIs.pathPrefixMatch(path, "/api/v2/incremental/")) {
			// unsure if they actually enforce this
			String id = "incremental_exports";
			return new RateLimitValue(id, 10, Duration.ofMinutes(1));
		}
		String id = "default";
		if (highVolumeAddOnEnabled)
			return new RateLimitValue(id, 2500, Duration.ofMinutes(1));
		else
			return new RateLimitValue(id, 200, Duration.ofMinutes(1));
	}

	public static class RateLimitValue {

		public final String id;
		public final int permits;
		public final Duration leaseDuration;

		public RateLimitValue(String id, int permits, Duration leaseDuration) {
			super();
			this.id = id;
			Validate.isTrue(permits >= 1);
			this.permits = permits;
			this.leaseDuration = Objects.requireNonNull(leaseDuration);
		}

	}

}
