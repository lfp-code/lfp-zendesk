package test;

import java.io.IOException;

import com.lfp.joe.serial.Serials;
import com.lfp.zendesk.client.ZDClient;
import com.lfp.zendesk.client.Zendesks;

public class TestEmail {

	public static void main(String[] args) throws IOException {
		var tid = 212407;
		var auditId = 816764808771l;
		ZDClient zdClient = Zendesks.get();
		zdClient.streamTicketEventContexts(tid).forEach(v -> {
			System.out.println(v.getAudit().getId() + " - " + v.getEvent().getClass().getName());
		});
		for (int i = 0; i < 2; i++) {
			var result = zdClient.getEmailMessage(tid, auditId).orElse(null);
			System.out.println(Serials.Gsons.getPretty().toJson(result));
		}
		System.exit(0);
	}

}
