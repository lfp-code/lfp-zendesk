package test;

import java.util.function.Function;

import org.zendesk.client.v2.model.Ticket;

import com.lfp.joe.stream.EntryStreams;
import com.lfp.joe.stream.Streams;
import com.lfp.zendesk.client.Zendesks;

public class StreamTicketsTest {

	public static void main(String[] args) {
		var ible = Zendesks.get().getTicketsFromSearch("activision");
		var tidStream = Streams.of(ible).map(Ticket::getId).distinct();
		var estream = tidStream.chain(EntryStreams.indexed()).map(Function.identity());
		estream.peekLast(ent -> {
			System.err.println(ent);
		}).forEach(ent -> {
			if (ent.getKey() % 1000 == 0)
				System.out.println(ent);
		});
	}

}
